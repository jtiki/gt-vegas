BEGIN;
--
-- Create model GTDPride
--
CREATE TABLE public.pride_gtdpride ("id" bigserial NOT NULL PRIMARY KEY, "guild_id" bigint NOT NULL, "user_id" bigint NOT NULL, "username" varchar(255) NOT NULL, "user_pfp" text NOT NULL, "message_id" bigint NOT NULL UNIQUE, "pride_message" text NOT NULL, "related_id" bigint NULL);
ALTER TABLE public.pride_gtdpride ADD CONSTRAINT "pride_gtdpride_related_id_438cc7e0_fk_pride_gtdpride_id" FOREIGN KEY ("related_id") REFERENCES public.pride_gtdpride ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "pride_gtdpride_guild_id_e84a276e" ON public.pride_gtdpride ("guild_id");
CREATE INDEX "pride_gtdpride_user_id_1f5cc7ab" ON public.pride_gtdpride ("user_id");
CREATE INDEX "pride_gtdpride_related_id_438cc7e0" ON public.pride_gtdpride ("related_id");
COMMIT;


BEGIN;
--
-- Add field pride_attachments to gtdpride
--
ALTER TABLE public.pride_gtdpride ADD COLUMN "pride_attachments" text NULL;
COMMIT;

