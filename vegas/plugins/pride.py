from checks import requires_postgres, setup_check_postgres
from discord import Embed, RawReactionActionEvent
from discord.ext import commands
from discord.ext.commands import MemberConverter, TextChannelConverter
from settings.base import APPROVED_ROLES, COLORS

from .base import BasePlugin

ADD_PRIDE = "INSERT INTO pride_gtdpride(guild_id, user_id, username, user_pfp, message_id, pride_message, pride_attachments) VALUES (%s, %s, %s, %s, %s, %s, %s);"
SELECT_PRIDE = "SELECT * FROM pride_gtdpride WHERE guild_id=%s ORDER BY id OFFSET %s ROWS FETCH NEXT 25 ROWS ONLY;"


class PridePlugin(BasePlugin):
    @commands.group()
    async def pride(self, context):
        pass

    @pride.command()
    async def list(self, context, page: int = 1):
        pride_chan = await TextChannelConverter().convert(
            ctx=context, argument="pride-and-coming-out"
        )
        offset = 0

        if page >= 1:
            offset = int(page - 1) * 25
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(
                    SELECT_PRIDE,
                    (
                        context.guild.id,
                        offset,
                    ),
                )
                pride_stories = await cursor.fetchall()

        if not pride_stories:
            await context.send("no stories")

            return
        embed = Embed(
            title=f"Pride Stories, Page {page}", description="", color=COLORS["ban"]
        )
        embed.set_thumbnail(url=context.guild.icon_url)

        for story in pride_stories:
            story_id = story[0]
            member = await MemberConverter().convert(
                ctx=context, argument=str(story[2])
            )
            field_value = f"{member.mention} - "

            if story[7]:
                field_value += "Related: ✅ - "
            message = await pride_chan.fetch_message(story[5])
            message_attachments = story[8]

            if not message_attachments:
                message_attachments = list()
            elif ", " in message_attachments:
                message_attachments = message_attachments.split(", ")
            elif message_attachments:
                message_attachments = [
                    message_attachments,
                ]
            else:
                message_attachments = list()

            attachment_count = "✅" * len(message_attachments)
            field_value += (
                f"[Jump to Message]({message.jump_url}) - {attachment_count} - "
            )
            remaining = 255 - len(field_value)
            field_value += f"{message.content[:remaining]}"
            embed.add_field(
                name=f"{member.name}'s Story - ID {story_id}",
                value=field_value,
                inline=False,
            )
        await context.send(embed=embed)

    @commands.Cog.listener("on_raw_reaction_add")
    async def add_to_db(self, payload: RawReactionActionEvent):
        if not payload.guild_id:
            return

        if payload.emoji.name != "🏳️‍🌈":
            return

        channel = self.bot.get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)

        if message.channel.name != "pride-and-coming-out":
            return

        if not any(
            user_role.name in APPROVED_ROLES for user_role in payload.member.roles
        ):
            return

        attachments = [attachment.proxy_url for attachment in message.attachments]
        attachments = ", ".join(attachments)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(
                    ADD_PRIDE,
                    (
                        message.guild.id,
                        payload.member.id,
                        f"{message.author.name}#{message.author.discriminator}",
                        str(message.author.avatar_url),
                        message.id,
                        message.content,
                        attachments,
                    ),
                )
        await channel.send(
            content="Thanks for your story!!", reference=message, mention_author=False
        )


def setup(bot):
    setup_check_postgres(bot, __file__)
    bot.add_cog(PridePlugin(bot))
