import datetime
import re
import typing
from typing import List, Optional, Union

from checks import (
    requires_postgres,
    requires_redis,
    setup_check_postgres,
    setup_check_redis,
)
from discord import Embed, Member, Role, TextChannel
from discord.ext import commands
from discord.ext.commands import (
    Context,
    MemberConverter,
    MissingRequiredArgument,
    RoleConverter,
    TextChannelConverter,
    has_any_role,
)
from discord.ext.commands.errors import MemberNotFound, RoleNotFound
from discord.utils import get
from settings.base import ADMIN_MAX_EMOJI, ADMIN_MAX_MENTIONS
from settings.base import APPROVED_ROLES as STRIKE_ROLES
from settings.base import (
    BAN_APPEAL,
    CHANNEL_PERM_DICT,
    COLORS,
    EVERYONE_PATTERN,
    HERE_PATTERN,
    URL_PATTERN,
)
from utils import convert_shorthand_to_datetime

from plugins.base import BasePlugin

TIMEOUT = 40

COLOR = COLORS["ban"]

LOG_CHANNEL_NAME = "vegas-logging"

WARN_SQL = "SELECT * FROM discord_user_discordwarn WHERE discord_user_id=%s ORDER BY date_recieved DESC LIMIT %s;"
ROLE_SQL = "SELECT * FROM discord_user_discordrole WHERE discord_user_id=%s ORDER BY date_recieved DESC LIMIT %s;"
STRIKE_SQL = "SELECT * FROM discord_user_discordstrike WHERE discord_user_id=%s ORDER BY date_recieved DESC LIMIT %s;"
KICK_SQL = "SELECT * FROM discord_user_discordkick WHERE discord_user_id=%s ORDER BY date_recieved DESC LIMIT %s;"
BAN_SQL = "SELECT * FROM discord_user_discordban WHERE discord_user_id=%s ORDER BY date_recieved DESC LIMIT %s;"
WARN_MEMBER_SQL = "INSERT INTO discord_user_discordwarn(date_recieved, reason, discord_user_id) VALUES(%s, %s, %s);"
STRIKE_MEMBER_SQL = "INSERT INTO discord_user_discordstrike(date_recieved, reason, strike_type, discord_user_id) VALUES(%s, %s, %s, %s);"
KICK_MEMBER_SQL = "INSERT INTO discord_user_discordkick(date_recieved, reason, discord_user_id) VALUES(%s, %s, %s);"
BAN_MEMBER_SQL = "INSERT INTO discord_user_discordban(date_recieved, reason, discord_user_id) VALUES(%s, %s, %s);"
DFORMAT = "%m/%d/%Y %I:%M %p %Z"


class AdminPlugin(BasePlugin):
    def __init__(self, bot):
        self.bot = bot

    async def cog_command_error(self, context, error):
        """Error handler for warn"""

        if isinstance(error, MissingRequiredArgument):
            arg = error.param.name
            error_type = arg.split("_")[0]

            if "member" in arg:
                await context.send(f"Please provide a member to {error_type}!")
            elif "reason" in arg:
                await context.send(f"Please provide a reason to {error_type}!")
            else:
                await context.send(f"Missing parameter: {arg}.")
        elif isinstance(error, MemberNotFound):
            await context.send(
                f"I tried to find the member `{error.argument}`, but couldn't."
            )
        elif isinstance(error, RoleNotFound):
            await context.send(
                f"I tried to find the role `{error.argument}` for a strike, but couldn't."
            )
        else:
            raise error

    def event_logging_channel(self, channels: List[TextChannel]) -> TextChannel:
        return get(channels, name=LOG_CHANNEL_NAME)

    async def _build_single_embed(
        self, context: Context, user: Member, action: str, reason: str
    ) -> Embed:
        if action == "warn":
            title = "{} has been warned!".format(user.name)
            field_name = "Warning Reason"

        if action == "strike":
            title = "{} has been struck!".format(user.name)
            field_name = "Strike Reason"

        if action == "kick":
            title = "{} has been kicked!".format(user.name)
            field_name = "Kick Reason"

        if action == "ban":
            title = "{} has been banned!".format(user.name)
            field_name = "Ban Reason"
        embed = Embed(title=title, color=COLOR)
        embed.add_field(name=field_name, value=reason, inline=False)
        embed.set_author(name=user.name, icon_url=user.avatar_url)
        # embed.set_footer(text="We're watching you :angrysquint:", icon_url=context.guild.icon)
        embed.set_footer(text="We're watching you")

        return embed

    async def _validate_member(self, context: Context, user) -> Member:
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            await context.send(f"I couldn't find member {user} not found!")
            user = await self._collect_response(context, "", commands.MemberConverter)

        return user

    async def _validate_reason(
        self, context: Context, reason: Union[str, List[str]]
    ) -> str:
        if reason:
            reason = " ".join(reason)
        else:
            reason = await self._collect_response(context, "reason", "string")

        return reason

    @commands.Cog.listener(name="on_message")
    async def catch_everyone_ping(self, message):
        """Listener to handle blocking and warning for @everyone"""

        if message.author.bot or not message.guild:
            return
        is_mod = get(message.author.roles, name="Mods")

        if is_mod:
            return

        if EVERYONE_PATTERN.search(message.content.lower()) or HERE_PATTERN.search(
            message.content.lower()
        ):

            context = await self.bot.get_context(message)
            warn_cmd = self.bot.get_command("warn")
            warn_message = await context.invoke(
                warn_cmd,
                message.author,
                echo_channel=None,
                warn_reason="Only Moderators can ping `@everyone`",
            )
            updated_embed = warn_message.embeds[0]
            updated_embed.set_image(
                url="https://cdn.discordapp.com/attachments/725789016590385179/762707287843536936/unknown.png"
            )
            await warn_message.edit(embed=updated_embed)

    @commands.Cog.listener(name="on_message")
    async def catch_invites(self, message):
        """Listener to handle blocking and warning for discord invites."""

        if message.author.id not in (
            691420795473494046,
            376171158497918976,
            692110917533696010,
            227117043143540736,
        ):
            link_regex = re.compile(
                r"(?:https?://)?discord(?:app\.com/invite|\.gg)/?[a-zA-Z0-9]+/?",
                re.DOTALL,
            )
            message_links = re.findall(link_regex, message.content)
            # await message.channel.send(message_links)

            for element in message_links:
                if "discord.gg" in element or "discord.com/invite" in element:
                    link_invite = await self.bot.fetch_invite(element)

                    if link_invite.guild.id != 353694095220408322:
                        context = await self.bot.get_context(message)
                        warn_cmd = self.bot.get_command("warn")
                        await context.invoke(
                            warn_cmd,
                            message.author,
                            echo_channel=None,
                            warn_reason="We do not allow discord invites!",
                        )
                        embed = Embed(
                            title="Discord Invite Deleted",
                            color=COLOR,
                            description=message.author.mention
                            + "has posted a discord invite!",
                        )
                        embed.add_field(name="Invite Link", value=element, inline=True)
                        embed.set_author(
                            name=message.author.name, icon_url=message.author.avatar_url
                        )
                        embed.set_footer(text="We're watching you")

                        await message.delete()
                        await self.event_logging_channel(
                            context.message.guild.text_channels
                        ).send(embed=embed)

    @commands.Cog.listener(name="on_message")
    async def catch_to_many_mentions(self, message):
        """"""
        if not message.guild:
            return
        if message.author.bot:
            return

        guild_mentions_max: int = await self.bot.redis.get(
            f"{message.guild.id}_MAX_MENTIONS", encoding="utf-8"
        )

        if not guild_mentions_max:
            guild_mentions_max = ADMIN_MAX_MENTIONS
        mentions = len(message.mentions) + len(message.role_mentions)

        if mentions >= int(guild_mentions_max):
            await message.channel.send("More than Max Mentions")
            context = await self.bot.get_context(message)
            warn_cmd = self.bot.get_command("warn")

            await context.invoke(
                warn_cmd,
                member=message.author,
                echo_channel=None,
                warn_reason="Spammed Mentions",
            )

    @commands.group()
    @has_any_role("Mods")
    async def admin(self, context):
        """Top level commnd for Admin Settings.

        Displays an embed with all admin settings for this guild."""

        if context.invoked_subcommand:
            return
        embed = Embed(
            title="AdminPlugin Setting(s)",
            description="Below are the various setting(s) for this plugin",
            color=COLOR,
        )
        guild_mentions_max = await self.bot.redis.get(
            f"{context.guild.id}_MAX_MENTIONS", encoding="utf-8"
        )
        guild_emoji_max = await self.bot.redis.get(
            f"{context.guild.id}_MAX_EMOJIS", encoding="utf-8"
        )

        if not guild_mentions_max:
            guild_mentions_max = ADMIN_MAX_MENTIONS

        if not guild_emoji_max:
            guild_emoji_max = ADMIN_MAX_EMOJI

        embed.add_field(name="Maximum Mention:", value=guild_mentions_max)
        embed.add_field(name="Maximum Emoji:", value=guild_emoji_max)

        await context.send(embed=embed)

    @admin.command()
    async def set_max_mentions(self, context, max_mentions: int):
        """Allows Mods to set the maximum number of mentions before a warn is automatically issued.

        Parameters
        ==========
        max_mentions: int
            A number to use for the max"""
        await self.bot.redis.set(f"{context.guild.id}_MAX_MENTIONS", max_mentions)
        await context.send(f"Max Mentions updated to {max_mentions}")

    @admin.command()
    async def set_max_emoji(self, context, max_emojis: int):
        """Allows Mods to set the maximum number of emoji before a warn is automatically issued.

        Parameters
        ==========
        max_emojis: int
            A number to use for the max"""
        await self.bot.redis.set(f"{context.guild.id}_MAX_EMOJIS", max_emojis)
        await context.send(f"Max Emojis updated to {max_emojis}")

    @commands.group(
        description="Check a users Warns/Kicks/Roles/Bans",
        case_insensitive=True,
        aliases=[
            "u",
        ],
    )
    @has_any_role("Can Warn")
    @requires_postgres()
    async def user(self, context):
        if context.invoked_subcommand is None:
            guild = context.guild
            member = context.message.mentions[0]
            fmt = "{date} - **{reason}**\n"

            link_id = await self._check_user_exists(member.id, guild.id)

            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cursor:
                    await cursor.execute(WARN_SQL, (link_id, 5))
                    warns = await cursor.fetchall()

                    await cursor.execute(STRIKE_SQL, (link_id, 5))
                    strikes = await cursor.fetchall()

                    await cursor.execute(ROLE_SQL, (link_id, 5))
                    roles = await cursor.fetchall()

                    await cursor.execute(KICK_SQL, (link_id, 3))
                    kicks = await cursor.fetchall()

                    await cursor.execute(BAN_SQL, (link_id, 2))
                    bans = await cursor.fetchall()

            embed = Embed(title="{} Infractions".format(member.name), color=COLOR)
            embed.set_thumbnail(url=member.avatar_url_as(static_format="png"))

            embed.add_field(name="Total Warns:", value=str(len(warns)), inline=True)
            embed.add_field(name="Total Strikes:", value=str(len(strikes)), inline=True)
            embed.add_field(name="Total Roles:", value=str(len(roles)), inline=True)
            embed.add_field(name="Total Kicks:", value=str(len(kicks)), inline=True)
            embed.add_field(name="Total Bans:", value=str(len(bans)), inline=True)

            fmt_str = "[None]"

            for warn in warns:
                if len(fmt_str) <= 1000:
                    date = warn[1].strftime(DFORMAT)

                    if fmt_str == "[None]":
                        fmt_str = fmt.format(date=date, reason=warn[2])
                    else:
                        fmt_str += fmt.format(date=date, reason=warn[2])
            embed.add_field(name="Warns", value=fmt_str, inline=False)

            fmt_str = "[None]"

            for strike in strikes:
                if len(fmt_str) <= 1000:
                    strike_channel = guild.get_role(int(strike[3]))
                    date = strike[1].strftime(DFORMAT)
                    strike_reason = strike_channel.name + " - " + strike[2]

                    if fmt_str == "[None]":
                        fmt_str = fmt.format(date=date, reason=strike_reason)
                    else:
                        fmt_str += fmt.format(date=date, reason=strike_reason)
            embed.add_field(name="Strikes", value=fmt_str, inline=False)

            fmt_str = "[None]"

            for role in roles:
                if "requested" not in role[2].lower():
                    if len(fmt_str) <= 1000:
                        date = role[1].strftime(DFORMAT)
                        grole = guild.get_role(int(role[4]))
                        date = role[3].strftime(DFORMAT)

                        if fmt_str == "[None]":
                            if role[6]:
                                fmt_str = "**++{}** {}\n".format(grole.name, date)
                            else:
                                fmt_str = "**--{}** on {}\n".format(grole.name, date)
                        else:
                            if role[6]:
                                fmt_str += "**++{}** {}\n".format(grole.name, date)
                            else:
                                fmt_str += "**--{}** on {}\n".format(grole.name, date)

            embed.add_field(name="Roles", value=fmt_str, inline=False)

            fmt_str = "[None]"

            for kick in kicks:
                if len(fmt_str) <= 1000:
                    date = kick[1].strftime(DFORMAT)

                    if fmt_str == "[None]":
                        fmt_str = fmt.format(date=date, reason=kick[2])
                    else:
                        fmt_str += fmt.format(date=date, reason=kick[2])

            embed.add_field(name="Kicks", value=fmt_str, inline=False)

            fmt_str = "[None]"

            for ban in bans:
                if len(fmt_str) <= 1000:
                    date = ban[1].strftime("%m-%d-%Y %I:%M %p %Z")

                    if fmt_str == "[None]":
                        fmt_str = fmt.format(date=date, reason=ban[2])
                    else:
                        fmt_str += fmt.format(date=date, reason=ban[2])

            embed.add_field(name="Bans:", value=fmt_str, inline=False)

            await context.send(embed=embed)

    @user.command(
        description="Check a users Roles",
        case_insensitive=True,
        aliases=[
            "ur",
        ],
    )
    @has_any_role("Can Warn")
    async def user_roles(self, context, username):
        guild = context.guild
        member = context.message.mentions[0]

        link_id = await self._check_user_exists(member.id, guild.id)

        embed = Embed(title="{} Warns".format(member.name), color=COLOR)
        embed.set_thumbnail(url=member.avatar_url)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(ROLE_SQL, (link_id, 25))
                roles = await cursor.fetchall()

        for role in roles:
            if "requested" not in role[2].lower():
                grole = guild.get_role(int(role[4]))
                date_recieved = role[1].strftime(DFORMAT)
                date_removed = role[3].strftime(DFORMAT)
                reason_tpl = "*Recieved:* {}\n*Removed:* {}\n**Reason:**\n{}"
                reason = reason_tpl.format(date_recieved, date_removed, role[2])

            embed.add_field(name=grole.name, value=reason, inline=False)

        await context.send(embed=embed)

    @user.command(
        description="Check a users Warns",
        case_insensitive=True,
        aliases=[
            "uw",
        ],
    )
    @has_any_role("Can Warn")
    async def user_warns(self, context, username):
        guild = context.guild
        member = context.message.mentions[0]

        link_id = await self._check_user_exists(member.id, guild.id)

        embed = Embed(title="{} Warns".format(member.name), color=COLOR)
        embed.set_thumbnail(url=member.avatar_url)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(WARN_SQL, (link_id, 25))
                warns = await cursor.fetchall()

        for warn in warns:
            date = warn[1].strftime(DFORMAT)
            embed.add_field(name=date, value=warn[2], inline=False)

        await context.send(embed=embed)

    @commands.command(
        description="Check a users strikes",
        case_insensitive=True,
        aliases=[
            "ls",
        ],
    )
    @has_any_role(*STRIKE_ROLES)
    @requires_postgres()
    async def user_strikes(self, context, username):
        guild = context.guild
        member = context.message.mentions[0]
        fmt = "{role} - {date}"
        link_id = await self._check_user_exists(member.id, guild.id)

        embed = Embed(title="{} Strikes".format(member.name), color=COLOR)
        embed.set_thumbnail(url=member.avatar_url)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(STRIKE_SQL, (link_id, 25))
                strikes = await cursor.fetchall()

        for strike in strikes:
            strike_role = guild.get_role(int(strike[3]))
            date = strike[1].strftime(DFORMAT)
            field_name = fmt.format(role=strike_role.name, date=date)
            embed.add_field(name=field_name, value=strike[2], inline=False)

        await context.send(embed=embed)

    @commands.command(
        description="@Can Warn command to interacatively warn a user.",
        case_insensitive=True,
        aliases=[
            "iw",
        ],
    )
    @has_any_role("Can Warn")
    @requires_postgres()
    async def interactive_warn(
        self,
        context,
        user: Optional[Member],
        echo_channel: Optional[TextChannel],
        *,
        reason: Optional[str],
    ):
        """Guides any member with Can Warn through the process of building a warn.

        Parameters
        ----------
        context: Context
            Defalt discord Context
        user: Optional[Member]
            The Member to warn.
        echo_channel: Optional[TextChannel]
            The TextChannel to post an update embed to.
        reason: Optional[str]
            The reason the user is warned.

        Notes
        -----
        - Invokes admin.warn after validating input"""
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            user = await self._collect_response(context, "", commands.MemberConverter)

            if not user:
                return
        try:
            echo_channel = await TextChannelConverter().convert(
                ctx=context, argument=echo_channel
            )
        except (commands.ChannelNotFound, TypeError):
            echo_channel = await self._collect_response(
                context, "", commands.TextChannelConverter
            )

        if isinstance(echo_channel, TextChannel):
            echo_channel_mention = echo_channel.mention
        else:
            echo_channel_mention = "-"
        reason = await self._validate_reason(context, reason)

        if not reason:
            return
        confirm_message = await context.send(
            f"{user.mention} will be warned in {echo_channel_mention} for `{reason}`?  :thumbsup: to confirm :x: to cancel."
        )

        confimed = await self._confirm_with_reaction(context, confirm_message)

        if confimed:
            return

        warn_cmd = self.bot.get_command("warn")

        await context.invoke(
            warn_cmd, member=user, echo_channel=echo_channel, warn_reason=reason
        )

    @commands.command(
        description="@Can Warn command to warn a user",
        case_insensitive=True,
        aliases=[
            "w",
        ],
    )
    @has_any_role("Can Warn")
    @requires_postgres()
    async def warn(
        self,
        context: Context,
        member: Member,
        echo_channel: Optional[TextChannel],
        *,
        warn_reason: str,
    ):
        """Warns a Member of the guild and records it in the DB.

        Parameters
        ----------
        context: Context
            Default discord Context
        member: Member
            The Member to be warned.
        echo_channel: Optional[TextChannel]
            The TextChannel to post an update embed to.
        warn_reason: str
            The reason the user was warned."""
        guild = context.guild
        now = datetime.datetime.now()
        link_id = await self._check_user_exists(member.id, guild.id)

        if context.message:
            await context.message.delete()

        if context.message.author.top_role.position < member.top_role.position:
            await context.send("You cannot warn someone with a higher role!")

            return

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(WARN_MEMBER_SQL, (now, warn_reason, link_id))
        embed = await self._build_single_embed(context, member, "warn", warn_reason)
        warn_message = await context.send(embed=embed)

        if echo_channel:
            await echo_channel.send(content=member.mention, embed=embed)

        return warn_message

    @commands.command(
        description="Any Approved can run this command to interatively strike a user",
        case_insensitive=True,
        aliases=[
            "is",
        ],
    )
    @has_any_role(*STRIKE_ROLES)
    @requires_postgres()
    async def interactive_strike(
        self,
        context: Context,
        user: Optional[Member],
        strike_type: Optional[Role],
        echo_channel: Optional[TextChannel],
        *,
        reason: Optional[str],
    ):
        """Provides an interactive way for Approved members to strike against a role.

        Parameters
        ----------
        context: Context
            Default discord Context
        user: Optional[Member]
            The Member to be struck.
        strike_type: Optional[Role]
            The role to be struck against.
        echo_channel: Optional[TextChannel]
            The TextChannel an update embed should be sent to.
        reason: Optional[str]
            The reason the Member has been struck.

        Notes
        -----
        - After validating input, invokes admin.strike"""
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            await context.send(f"I couldn't find member {user} not found!")
            user = await self._collect_response(context, "", commands.MemberConverter)

            if not user:
                return
        try:
            strike_type = await RoleConverter().convert(ctx=context, argument=user)
        except (commands.RoleNotFound, TypeError):
            await context.send(f"I couldn't find role {strike_type} not found!")
            strike_type = await self._collect_response(
                context, "", commands.RoleConverter
            )

            if not strike_type:
                return
        try:
            echo_channel = await TextChannelConverter().convert(
                ctx=context, argument=echo_channel
            )
        except (commands.ChannelNotFound, TypeError):
            await context.send(f"I couldn't find channel {echo_channel} not found!")
            echo_channel = await self._collect_response(
                context, "", commands.TextChannelConverter
            )

        if isinstance(echo_channel, TextChannel):
            echo_channel_mention = echo_channel.mention
        else:
            echo_channel_mention = "-"
        reason = await self._validate_reason(context, reason)

        if not reason:
            return
        confirm_message = await context.send(
            f"{user.mention} will be struck in {echo_channel_mention} for `{reason}`. :thumbsup: to confirm :x: to cancel."
        )

        confimed = await self._confirm_with_reaction(context, confirm_message)

        if confimed:
            return

        strike_cmd = self.bot.get_command("strike")
        await context.invoke(strike_cmd, user, strike_type, echo_channel, reason)

    @commands.command(
        description="Any Approved can run this command to strike a user",
        case_insensitive=True,
        aliases=[
            "s",
        ],
    )
    @has_any_role(*STRIKE_ROLES)
    @requires_postgres()
    async def strike(
        self,
        context: Context,
        member: Member,
        strike_type: Role,
        echo_channel: Optional[TextChannel],
        *,
        strike_reason: str,
    ):
        """Provides a way for Approved members to strike against a role.

        Parameters
        ----------
        context: Context
            Default discord Context
        user: Optional[Member]
            The Member to be struck.
        strike_type: Optional[Role]
            The role to be struck against.
        echo_channel: Optional[TextChannel]
            The TextChannel an update embed should be sent to.
        reason: Optional[str]
            The reason the Member has been struck."""
        guild = context.guild
        now = datetime.datetime.now()
        link_id = await self._check_user_exists(member.id, guild.id)

        await context.message.delete()

        if context.message.author.top_role.position < member.top_role.position:
            await context.send("You cannot strike someone with a higher role!")

            return

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(
                    STRIKE_MEMBER_SQL, (now, strike_reason, strike_type.id, link_id)
                )
        embed = await self._build_single_embed(context, member, "strike", strike_reason)

        await context.send(embed=embed)

        if echo_channel:
            await echo_channel.send(content=member.mention, embed=embed)

    @commands.command(
        description="@Can Kick command to Kick a user interactively",
        case_insensitive=True,
        aliases=[
            "ik",
        ],
    )
    @has_any_role("Can Kick")
    @requires_postgres()
    async def interactive_kick(
        self, context, user=None, echo_channel=None, *reason: typing.Optional
    ):
        """Provides an interactive way for Can Kick members to Kick a Member from the build.

        Parameters
        ----------
        context: Context
            Default discord Context
        user: Optional[Member]
            The Member to be struck.
        echo_channel: Optional[TextChannel]
            The TextChannel an update embed should be sent to.
        reason: Optional[str]
            The reason the Member has been struck.

        Notes
        -----
        - After validating input, invokes admin.kick"""
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            await context.send(f"I couldn't find member {user} not found!")
            user = await self._collect_response(context, "", commands.MemberConverter)

        if not user:
            return
        try:
            echo_channel = await TextChannelConverter().convert(
                ctx=context, argument=echo_channel
            )
        except (commands.ChannelNotFound, TypeError):
            await context.send(f"I couldn't find channel {echo_channel} not found!")
            echo_channel = await self._collect_response(
                context, "", commands.TextChannelConverter
            )

        if isinstance(echo_channel, TextChannel):
            echo_channel_mention = echo_channel.mention
        else:
            echo_channel_mention = "-"
        reason = await self._validate_reason(context, reason)

        if not reason:
            return
        confirm_message = await context.send(
            f"{user.mention} will be kicked in {echo_channel_mention} for `{reason}`. :thumbsup: to confirm :x: to cancel."
        )

        confimed = await self._confirm_with_reaction(context, confirm_message)

        if confimed:
            return

        kick_cmd = self.bot.get_command("kick")
        await context.invoke(
            kick_cmd, member=user, echo_channel=echo_channel, reason=reason
        )

    @commands.command(
        description="@Can Kick command to Kick a user",
        case_insensitive=True,
        aliases=[
            "k",
        ],
    )
    @has_any_role("Can Kick")
    @requires_postgres()
    async def kick(
        self,
        context: Context,
        member: Member,
        echo_channel: Optional[TextChannel],
        *,
        kick_reason: str,
    ):
        """Kicks a Member from the guild.
        Parameters
        ----------
        context: Context
            Default discord Context
        member: Member
            The Member to kick.
        echo_channel: Optional[TextChannel]
            The TextChannel to post an update embed to.
        *reason: str
            The reason the Member was kicked.
        """
        guild = context.guild
        now = datetime.datetime.now()
        link_id = await self._check_user_exists(member.id, guild.id)
        reason = f"{kick_reason} - You have been kicked from {context.guild.name}. If you are unable to rejoin, contact Discord."

        await context.message.delete()

        if context.message.author.top_role.position < member.top_role.position:
            await context.send("You cannot kick someone with a higher role!")

            return

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await member.send(reason)
                await member.kick(reason=reason)
                await cursor.execute(KICK_MEMBER_SQL, (now, reason, link_id))
        embed = await self._build_single_embed(context, member, "kick", reason)
        await context.send(embed=embed)

        if echo_channel:
            await echo_channel.send(content=member.mention, embed=embed)

    @commands.command(
        description="@Can Ban command to Ban a user interactively",
        case_insensitive=True,
        aliases=[
            "ib",
        ],
    )
    @has_any_role("Can Ban")
    @requires_postgres()
    async def interactive_ban(
        self,
        context,
        user: Optional[Member],
        echo_channel: Optional[TextChannel],
        *reason: Optional[str],
    ):
        """Interactively ban a user.
        Each parameter is optional. Any parameters not provided or correct will be prompted
        for.

        Parameters:
        -----------
        user: Optional[Member]
            The Member to ban. If not provided, will be interactively collected.
        echo_channel: Optional[TextChannel]
            The TextChannel to post an update embed to. If not provided, will be interactively collected.
            Allows "-" as a skip.
        *reason: Optional[str]
            The reason this Member is being banned."""
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            await context.send(f"I couldn't find member {user} not found!")
            user = await self._collect_response(context, "", commands.MemberConverter)

        if not user:
            return
        try:
            echo_channel = await TextChannelConverter().convert(
                ctx=context, argument=echo_channel
            )
        except (commands.ChannelNotFound, TypeError):
            await context.send(f"I couldn't find channel {echo_channel} not found!")
            echo_channel = await self._collect_response(
                context, "", commands.TextChannelConverter
            )

        if isinstance(echo_channel, TextChannel):
            echo_channel_mention = echo_channel.mention
        else:
            echo_channel_mention = "-"
        reason = await self._validate_reason(context, reason)

        if not reason:
            return
        confirm_message = await context.send(
            f"{user.mention} will be banned in {echo_channel_mention} for `{reason}`. :thumbsup: to confirm :x: to cancel."
        )

        confimed = await self._confirm_with_reaction(context, confirm_message)

        if confimed:
            return

        ban_cmd = self.bot.get_command("ban")

        await context.invoke(
            ban_cmd, member=user, echo_channel=echo_channel, ban_reason=reason
        )

    @commands.command(
        description="@Can Ban command to ban a user",
        case_insensitive=True,
        aliases=[
            "b",
        ],
    )
    @has_any_role("Mods", "Can Ban")
    @requires_postgres()
    async def ban(
        self,
        context,
        member: Optional[Member],
        echo_channel: Optional[TextChannel],
        *,
        ban_reason: Optional[str],
    ):
        """Bans a `member` from the guild

        Parameters
        ----------
        context: Context
            Default discord Context
        member: Member
            The Member to ban.
        echo_channel: TextChannel
            The TextChannel to post the ban message to.
        *reason: List[str]
            The reason the user was banned.
        """
        guild = context.guild
        now = datetime.datetime.now()
        reason = f"{ban_reason} - Ban Appeal {BAN_APPEAL}"
        link_id = await self._check_user_exists(member.id, guild.id)
        ban_log_chan = await TextChannelConverter().convert(
            ctx=context, argument="ban-log"
        )

        if (
            context.message.author.id == 615735087635628063
            and member.id == 615735087635628063
        ):
            return

        await context.message.delete()

        if context.message.author.top_role.position < member.top_role.position:
            await context.send("You cannot ban someone with a higher role!")

            return

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await member.send(reason)
                await member.ban(reason=reason, delete_message_days=0)
                await cursor.execute(BAN_MEMBER_SQL, (now, reason, link_id))

        embed = await self._build_single_embed(context, member, "ban", reason)
        await context.send(embed=embed)
        await ban_log_chan.send(embed=embed)

        if echo_channel:
            await echo_channel.send(content=member.mention, embed=embed)

    @commands.command(
        description="Slow down the chat.",
        case_insensitive=True,
        aliases=[
            "slowmode",
        ],
    )
    @commands.has_any_role("Mods", "Disaster Director")
    async def slow(self, ctx: Context, timeout: Optional[str]):
        print(timeout)

        if not timeout:
            await ctx.send("Please provide a delay! (e.x. 5s = 5 seconds)")

        elif timeout[-1].isalpha() and timeout[0:-1].isdigit():
            removal_time = convert_shorthand_to_datetime(timeout)

            if removal_time:
                await ctx.channel.edit(slowmode_delay=removal_time.total_seconds())
                await ctx.send(
                    f":snail: {ctx.channel.mention} is now in slowmode. Users can only send messages every {removal_time.total_seconds()}s."
                )
            else:
                await ctx.send("Something went wrong converting that timeout...")

            return

        elif timeout == "off":
            await ctx.channel.edit(slowmode_delay=0)
            await ctx.send(f":rabbit2: {ctx.channel.mention} is no longer in slowmode.")

            return

        else:
            await ctx.send("You forgot the delay indicator after the number. >:(")

            return

    @commands.command(
        description="Lock someone in rulejail (for at least 30 minutes)",
        case_insensitive=True,
    )
    @commands.has_any_role("Mods", "Disaster Director")
    async def rulejail(self, ctx: Context, member: Member):

        if ctx.author.top_role < member.top_role:
            await ctx.send("You cannot send somebody with a higher role to rulejail.")
            return

        role_cmd = self.bot.get_command("notifications give")
        temp_role_cmd = self.bot.get_command("notifications temp_give")

        await ctx.invoke(temp_role_cmd, member, "30m", "Muted", "Go read the rules")
        await ctx.invoke(
            role_cmd, member, None, "Haven't Read the Rules", "Go read the rules"
        )

    @commands.command(
        description="Deletes all active invites.",
        case_insensitive=True,
        aliases=[
            "inv_del",
        ],
    )
    @commands.has_any_role("Mods", "Disaster Director")
    async def delete_invites(self, ctx: Context):
        """Lists and deletes all active invites (Mod and DD use only)"""
        invitelist = await ctx.guild.invites()
        for invite in invitelist:
            if not invite.revoked:
                await ctx.send(f"removing invite with code {invite.code}")
                await invite.delete()

    @commands.Cog.listener(name="on_message")
    async def enforce_perms(self, message):
        """Listener to check if the user has embed permissions when posting in specific channels"""
        check = False
        if re.match(URL_PATTERN, message.content):
            for channelname in CHANNEL_PERM_DICT.keys():
                if channelname == message.channel.name:
                    for role in message.author.roles:
                        if role.name in CHANNEL_PERM_DICT[message.channel.name]:
                            check = True
                    if not check:
                        server_info = self.bot.get_channel(735635802960429087)
                        await message.channel.send(
                            f"{message.author.mention} you do not have the required "
                            f"leveled role to post files or attach links in this channel. "
                            f"Please re-read the pinned rules and {server_info}"
                        )
                        await message.delete()
                    break


def setup(bot):
    """Simple setup for AdminPlugin ensuring postgres is setup or warning"""
    setup_check_postgres(bot, __file__)
    setup_check_redis(bot, __file__)
    bot.add_cog(AdminPlugin(bot))
