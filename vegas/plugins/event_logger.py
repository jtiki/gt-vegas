"""This module provides the EventLoggerPlugin"""
import re
from textwrap import wrap

import discord
from discord.ext import commands
from discord.utils import get
from settings.base import BAD_WORD_PATTERN, BAN_APPEAL, COLORS, EVERYONE_PATTERN
from settings.local_settings import IN_NICK, WORRY_CHECK
from utils import to_relative_delta

LOG_CHANNEL_NAME = "vegas-logging"
VC_LOG_CHANNEL_NAME = "vc-logging"
SPAM_LOG_CHANNEL_NAME = "bot-spam-logging"

IGNORED_CHANNELS = [
    "the-senate",
    "the-senate-bot-spam",
]


class EventLoggerPlugin(commands.Cog):
    """EventLoggerPlugin provides methods to log all events in a server"""

    def __init__(self, bot):
        self.bot = bot

    def event_logging_channel(self, guild):
        "Helper function that gets the event-logging channel from a list" ""

        return get(guild.text_channels, name=LOG_CHANNEL_NAME)

    def spam_logging_channel(self, guild):
        "Helper function that gets the spam-logging channel from a list" ""

        return get(guild.text_channels, name=SPAM_LOG_CHANNEL_NAME)

    def vc_logging_channel(self, guild):
        "Helper function that gets the vc-logging channel from a list" ""

        return get(guild.text_channels, name=VC_LOG_CHANNEL_NAME)

    def server_help_channel(self, guild):
        "Helper function that gets the server-help channel from a list" ""

        return get(guild.text_channels, name="server-help")

    def mods_ping(self, roles):
        """Helper function to get the Mods role from a list"""
        mod_role = get(roles, name="Mods")

        return mod_role

    def dds_ping(self, roles):
        """Helper function to get the Disaster Director role from a list"""
        dd_role = get(roles, name="Disaster Director")

        return dd_role

    def warn_ping(self, roles):
        """Helper function to get the Can Warn role from a list"""
        can_warn = get(roles, name="Can Warn")

        return can_warn

    def add_fields(self, matchlist, embed):
        for match in matchlist:
            pos = match.start()
            word = match.group()
            word = re.sub(r"[|,.\-_+#´`'?!()\{\}[\]\"\s]", "", word)
            word = word.lower()

            if len(embed) + len(word) < 5967:
                try:
                    if not embed.fields[23]:
                        embed.add_field(
                            name="Banned Word",
                            value=f'Found "{word}" at position {pos}.',
                        )
                    else:
                        embed.add_field(name="Banned Word", value="and more...")

                        return
                except IndexError:
                    embed.add_field(
                        name="Banned Word", value=f'Found "{word}" at position {pos}.'
                    )
            else:
                embed.add_field(name="Banned Word", value="and more...")

    @commands.Cog.listener()
    async def on_member_update(self, before: discord.Member, after: discord.Member):
        """Logs all member account updates in the server"""
        had_before = None
        had_after = None
        embed_color = COLORS["profile_change"]
        membername = "{membername}#{disc}".format(
            membername=before.name, disc=before.discriminator
        )
        embed_footer = "Author ID: {aid}".format(aid=before.id)
        roles = before.guild.roles
        mods_ping = self.mods_ping(roles)
        mods_ids = [member.id for member in mods_ping.members]
        dds_ping = self.dds_ping(roles)

        if dds_ping:
            dds_ids = [member.id for member in dds_ping.members]

        if before.roles != after.roles:
            before_set = set(before.roles)
            after_set = set(after.roles)
            had_before = before_set - after_set
            had_after = after_set - before_set
            log_format = "{}  ID: {}"

            if had_before:
                embed_color = COLORS["role_removed"]
            elif had_after:
                embed_color = COLORS["role_added"]

        embed = discord.Embed(description="Member Updated", color=embed_color)
        embed.set_thumbnail(url=before.avatar_url)
        embed.set_author(name=membername, icon_url=before.avatar_url)
        embed.set_footer(text=embed_footer)

        if before.nick != after.nick:
            before_nick = before.nick if before.nick != "" else "[None]"
            after_nick = after.nick if after.nick != "" else "[None]"

            if any(nick_catch in after.nick.lower() for nick_catch in IN_NICK):
                if after.id not in mods_ids or after.id not in dds_ids:
                    await self.event_logging_channel(before.guild).send(
                        "Suspicious Nickname {}".format(mods_ping.mention)
                    )

            if any(nick_catch in after.nick.lower() for nick_catch in WORRY_CHECK):
                if after.id not in mods_ids or after.id not in dds_ids:
                    watching = get(before.guild.roles, name="Watching")
                    await self.event_logging_channel(before.guild).send(
                        "Suspicious Nickname {} <{}> banned - {}".format(
                            after.name, after.id, watching.mention
                        )
                    )
                    breason = f"You have been banned on alt suspicion. If this was incorrect, please fill out this form: {BAN_APPEAL}"
                    await after.send(breason)
                    await after.ban(reason=breason)

            embed.add_field(name="Nick Before:", value=before_nick, inline=False)
            embed.add_field(name="Nick After:", value=after_nick, inline=False)

        if had_before:
            before_out = "\n".join(
                map(lambda x: log_format.format(x.name, x.id), had_before)
            )
            embed.add_field(name="Removed Roles:", value=before_out, inline=False)

        if had_after:
            after_out = "\n".join(
                map(lambda x: log_format.format(x.name, x.id), had_after)
            )
            embed.add_field(name="Added Roles:", value=after_out, inline=False)

        if len(embed.fields):
            await self.event_logging_channel(before.guild).send(embed=embed)

    @commands.Cog.listener()
    async def on_user_update(self, before: discord.Member, after: discord.Member):
        """Logs all user account updates in the server"""
        title = "Member Account {} Updated"
        color = COLORS["join"]
        before_field = None
        after_field = None
        use_icon = False
        """
        if after.avatar != before.avatar:
            title = title.format("Avatar")
            color = COLORS['avatar']
            before_field = before.avatar_url
            after_field = after.avatar_url
            use_icon = True
        """

        for guild in self.bot.guilds:
            if any(before.id == guser.id for guser in guild.members):
                if before.name != after.name:
                    title = title.format("Username")
                    color = COLORS["username"]
                    before_field = before.name
                    after_field = after.name

                if before_field:
                    embed_description = after.mention
                    embed = discord.Embed(
                        description=embed_description, title=title, color=color
                    )
                    embed.add_field(name="Before:", value=before_field, inline=False)
                    embed.add_field(name="After:", value=after_field, inline=False)

                    if use_icon:
                        embed.set_thumbnail(url=after_field)

                    await self.event_logging_channel(guild).send(embed=embed)

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        """Logs all voice state changes in the server"""
        membername = "{membername}#{disc}".format(
            membername=member.name, disc=member.discriminator
        )
        embed_description = "{mention} {membername}".format(
            mention=member.mention, membername=membername
        )
        embed_footer = "ID: {aid}".format(aid=member.id)

        embed = discord.Embed(description=embed_description, color=COLORS["voice"])
        embed.set_thumbnail(url=member.avatar_url)
        embed.set_author(name="Member VC Changed", icon_url=member.avatar_url)

        if before.mute is not after.mute:
            mute_status = "User Server Muted" if after.mute else "User Server Unmuted"
            embed.add_field(name="Mute", value=mute_status)

        if before.self_mute is not after.self_mute:
            self_mute_status = (
                "User Self Muted" if after.self_mute else "User Removed Self Mute"
            )
            embed.add_field(name="Self Mute", value=self_mute_status)

        if before.deaf is not after.deaf:
            deaf_status = "User Deafened" if after.deaf else "User UnDeafened"
            embed.add_field(name="deaf", value=deaf_status)

        if before.self_deaf is not after.self_deaf:
            self_deaf_status = (
                "User Self Defened" if after.self_deaf else "User Removed Self Deafen"
            )
            embed.add_field(name="Self Deafened", value=self_deaf_status)

        if before.self_stream is not after.self_stream:
            self_stream_status = (
                "User Started Streaming" if after.self_stream else "User Ended a Stream"
            )
            embed.add_field(name="Stream Status", value=self_stream_status)

        if before.self_video is not after.self_video:
            self_video_status = (
                "User Started Broadcasting videoo"
                if after.self_video
                else "User Ended Broadcasting videoo"
            )
            embed.add_field(name="Broadcast video", value=self_video_status)
        before_channel = "Joined VC "
        after_channel = " left VC"

        if before.channel is not None:
            before_channel = "Moved from {} ".format(before.channel.name)

        if before.channel is not after.channel:
            if after.channel is not None:
                after_channel = "to {}".format(after.channel.name)
            channel_status = before_channel + after_channel
            embed.add_field(name="Channel", value=channel_status)
        embed.set_footer(text=embed_footer)

        await self.vc_logging_channel(member.guild).send(embed=embed)

    @commands.Cog.listener()
    async def on_member_join(self, member):
        """Logs all joins in the server"""
        guild = member.guild
        roles = guild.roles
        c = member.created_at
        # returns tuple ((str) x time ago, (bool) <1 day, (bool) <1 week, (bool) age >1 week, (bool) age >1 month,
        # (bool) age >1 year)
        outdelta = to_relative_delta(c)

        membername = "{membername}#{disc}".format(
            membername=member.name, disc=member.discriminator
        )
        embed_description = "{mention} {membername}\nCreated {time}".format(
            mention=member.mention, membername=membername, time=outdelta[0]
        )
        embed_footer = "ID: {aid} - Account Created: {acreated}".format(
            aid=member.id, acreated=member.created_at
        )

        mods_ping = self.mods_ping(roles)
        mods_ids = [member.id for member in mods_ping.members]
        dds_ping = self.dds_ping(roles)

        if dds_ping:
            dds_ids = [member.id for member in dds_ping.members]

        # Ping mods for suspicious nickname

        if any(nick_catch in member.name.lower() for nick_catch in IN_NICK):
            if member.id not in mods_ids or member.id not in dds_ids:
                await self.event_logging_channel(member.guild).send(
                    "Suspicious Nickname {}".format(mods_ping.mention)
                )
        # Instantly ban users if they meet this criteria then ping watching

        if any(nick_catch in member.name.lower() for nick_catch in WORRY_CHECK):
            if member.id not in mods_ids or member.id not in dds_ids:
                watching = get(member.guild.roles, name="Watching")
                await self.event_logging_channel(member.guild).send(
                    "Suspicious Nickname {} <{}> banned - {}".format(
                        member.name, member.id, watching.mention
                    )
                )
                breason = f"You have been banned on alt suspicion. If this was incorrect, please fill out this form: {BAN_APPEAL}"
                await member.send(breason)
                await member.ban(reason=breason)

        embed = discord.Embed(description=embed_description, color=COLORS["join"])
        embed.set_thumbnail(url=member.avatar_url)
        embed.set_footer(text=embed_footer)
        embed.set_author(name="Member Joined", icon_url=member.avatar_url)
        # checks for special creation time frames. outdelta[2] and outdelta[3] check may not be needed, outdelta[4] could be removed too if wanted

        if outdelta[5]:
            embed.add_field(
                name="Very old Account",
                value="The Account was created over a year ago.",
            )
        elif outdelta[4]:
            embed.add_field(
                name="Old Account", value="The Account was created over a month ago."
            )
        elif outdelta[3]:
            embed.add_field(
                name="Relatively old Account",
                value="The Account was created over a week ago.",
            )
        elif outdelta[2]:
            embed.add_field(
                name="Somewhat new Account",
                value="The Account was created less than a week ago.",
            )
        elif outdelta[1]:
            embed.add_field(
                name="Very new Account",
                value="The Account was created less than one day ago.",
            )
        await self.event_logging_channel(member.guild).send(embed=embed)

        # Checks name for ascii text or bad words, if there are any, change
        # nickname to 'pingable nickname' or 'appropriate nickname' respectively

        if BAD_WORD_PATTERN.search(member.name):
            await member.edit(nick="appropriate nickname")
        """
        elif not member.name.isascii():
            await member.edit(nick='pingable nickname')
        """

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        """Logs all leaves in the server"""
        membername = "{membername}#{disc}".format(
            membername=member.name, disc=member.discriminator
        )
        embed_description = "{mention} {membername}".format(
            mention=member.mention, membername=membername
        )
        embed_footer = "ID: {aid}".format(aid=member.id)

        no_roles = ""

        for role in member.roles:
            if role.name.lower().startswith("no "):
                if no_roles == "":
                    no_roles = role.name + "\n"
                else:
                    no_roles += role.name + "\n"

        if no_roles == "":
            no_roles = "[None]"

        embed = discord.Embed(description=embed_description, color=COLORS["left"])
        embed.set_thumbnail(url=member.avatar_url)
        embed.set_footer(text=embed_footer)
        embed.set_author(name="Member Left", icon_url=member.avatar_url)
        embed.add_field(name="No Roles:", value=no_roles)
        await self.event_logging_channel(member.guild).send(embed=embed)

    @commands.Cog.listener()
    async def on_member_ban(self, guild, user):
        """Logs all bans in the server"""
        try:
            await user.send(f"lmao thanks for playing! {BAN_APPEAL}")
        except Exception:
            pass
        username = "{username}#{disc}".format(
            username=user.name, disc=user.discriminator
        )
        embed_description = "{mention} {username}".format(
            mention=user.mention, username=username
        )
        embed_footer = "ID: {aid}".format(aid=user.id)

        embed = discord.Embed(description=embed_description, color=COLORS["ban"])
        embed.set_thumbnail(url=user.avatar_url)
        embed.set_footer(text=embed_footer)
        embed.set_author(name="lmao gottem", icon_url=user.avatar_url)
        await self.event_logging_channel(guild).send(embed=embed)

    @commands.Cog.listener()
    async def on_member_unban(self, guild, user):
        """Logs all unbans in the server"""
        username = "{username}#{disc}".format(
            username=user.name, disc=user.discriminator
        )
        embed_description = "{mention} {username}".format(
            mention=user.mention, username=username
        )
        embed_footer = "ID: {aid}".format(aid=user.id)

        embed = discord.Embed(description=embed_description, color=COLORS["unban"])
        embed.set_thumbnail(url=user.avatar_url)
        embed.set_footer(text=embed_footer)
        embed.set_author(name="welcome back", icon_url=user.avatar_url)
        await self.event_logging_channel(guild).send(embed=embed)

    @commands.Cog.listener()
    async def on_bulk_message_delete(self, messages):
        """Logs all messages being bulk deleted in the server"""

        if messages[0].channel.name in IGNORED_CHANNELS or isinstance(
            messages[0].channel, discord.DMChannel
        ):
            return
        embed_description = "Bulk delete in {channel}, {x} messages deleted".format(
            channel=messages[0].channel.mention, x=len(messages)
        )

        embed = discord.Embed(description=embed_description, color=COLORS["delete"])

        if messages[0].channel.name == "bot-spam":
            await self.spam_logging_channel(messages[0].guild).send(embed=embed)
        else:
            await self.event_logging_channel(messages[0].guild).send(embed=embed)

    @commands.Cog.listener()
    async def on_message_delete(self, message):
        """Logs all messages being deleted in the server"""

        if isinstance(
            message.channel, discord.DMChannel
        ) or message.channel.name in IGNORED_CHANNELS:
            return

        if message.author.id == 227117043143540736:
            return
        message.content = message.content if message.content != "" else "[Empty]"
        embed_description = "Message sent by {user} deleted in {channel}".format(
            user=message.author.mention, channel=message.channel.mention
        )
        embed_footer = "Author ID: {aid} | Message ID: {mid}".format(
            aid=message.author.id, mid=message.id
        )
        username = "{username}#{disc}".format(
            username=message.author.name, disc=message.author.discriminator
        )
        embed = discord.Embed(description=embed_description, color=COLORS["delete"])
        embed.set_thumbnail(url=message.author.avatar_url)
        embed.set_footer(text=embed_footer)
        embed.set_author(name=username, icon_url=message.author.avatar_url)

        message_content = wrap(message.content, 1000)

        for field_value in message_content:
            embed.add_field(name="After Content:", value=field_value, inline=False)

        if (
            message.channel.name == "bot-spam"
            or message.channel.name == "approved-vegas-plz"
        ):
            await self.spam_logging_channel(message.guild).send(embed=embed)
        else:
            await self.event_logging_channel(message.guild).send(embed=embed)

    @commands.Cog.listener()
    async def on_message_edit(self, before, after):
        """Logs all messages being edited in the server"""

        if (
            before.content != ""
            and after.content != ""
            and not isinstance(before.channel, discord.DMChannel)
        ):
            if before.channel.name not in IGNORED_CHANNELS:
                after.content = after.content if after.content != "" else "[Deleted]"
                before.content = before.content if before.content != "" else "[Deleted]"
                embed_description = (
                    "Message edited in {channel} \n\n [Jump to Message]({link})".format(
                        channel=before.channel.mention, link=before.jump_url
                    )
                )
                embed_footer = "Author ID: {aid}".format(aid=before.author.id)
                username = "{username}#{disc}".format(
                    username=before.author.name, disc=before.author.discriminator
                )

                embed = discord.Embed(
                    description=embed_description, color=COLORS["edit"]
                )
                embed.set_thumbnail(url=before.author.avatar_url)
                embed.set_footer(text=embed_footer)
                embed.set_author(name=username, icon_url=before.author.avatar_url)

                before_content = wrap(before.content, 1000)

                for field_value in before_content:
                    embed.add_field(
                        name="Before Content:", value=field_value, inline=False
                    )

                after_content = wrap(after.content, 1000)

                for field_value in after_content:
                    embed.add_field(
                        name="After Content:", value=field_value, inline=False
                    )

                if before.channel.name == "bot-spam":
                    await self.spam_logging_channel(before.guild).send(embed=embed)
                else:
                    await self.event_logging_channel(before.guild).send(embed=embed)

    @commands.Cog.listener(name="on_message")
    async def bad_word_checker(self, message):
        """This event listener handles checking all incoming messages for a regex `BAD_WORD_PATTERN`.

        If a bad word is found, an embed is generated and sent to self.event_logging_channel."""

        if not message.guild:
            return

        bad_word_matches = BAD_WORD_PATTERN.search(message.content.lower())

        if bad_word_matches:
            message_content = message.content
            context = await self.bot.get_context(message)
            warn_cmd = self.bot.get_command("warn")
            await context.invoke(
                warn_cmd, message.author, echo_channel=None, warn_reason="Bad word used"
            )
            membername = f"{message.author.name}#{message.author.discriminator}"
            found_word = message_content[
                bad_word_matches.start() : bad_word_matches.end()
            ]
            field_val = f"{message_content[: bad_word_matches.start()]}***{found_word}***{message_content[bad_word_matches.end():]}"
            embed_description = (
                f"{message.author.mention} {membername}\n\nID: {message.author.id}"
            )

            embed = discord.Embed(
                title="Banned Word(s) Detected",
                description=embed_description,
                color=000000,
            )
            embed.add_field(
                name=f"Word Detected: {found_word}", value=field_val, inline=False
            )
            await self.event_logging_channel(message.guild).send(
                f"Bad words detected in {message.channel.mention} by {message.author.mention} (id: {message.author.id})",
                embed=embed,
            )


def setup(bot):
    """Basic setup for EventLoggerPlugin"""
    bot.add_cog(EventLoggerPlugin(bot))
