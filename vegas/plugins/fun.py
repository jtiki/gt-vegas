"""A random and fun module.

Developed by Deborah#6709
"""
import random
import discord
from discord.ext import commands
from typing import Optional
from .base import BasePlugin

RESPONSES = [
    "Ask again when I give a shit.",
    "Yes, if you leave me alone.",
    "Coward.",
    "Well, duh.",
    "Try again when I actually care.",
    "Cannot predict now, I'm sleepy.",
    "Actually think about what you are asking me and try again.",
    "My sources say no.",
    "No...just no.",
    "`[In energy saving mode. Try again never]`",
    "Yeah, Sure.",
    "Better not tell you now.",
    "Concentrate and ask again.",
    "Don't count on it.",
    "Outlook not so good.",
    "Very doubtful.",
    "Yes.",
    "Absolutely.",
    "Whatever.",
]


class FunPlugin(BasePlugin):
    """A set of fun commands!"""

    @commands.command(aliases=["8ball"])
    async def _8ball(self, context, *, _):
        """Takes a question and returns a random `RESPONSE`."""
        embed = discord.Embed(
            title=":8ball: 8ball",
            description=random.choice(RESPONSES),
            color=int("75c5f8", 16),
        )
        await context.send(embed=embed)

    @_8ball.error
    async def _8ball_error(self, ctx, error):
        """Error handler for _8ball"""
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(
                "You know you have to ask a question to get an answer, right?"
            )

    @commands.command(description="Gives profile picture",
                      case_insensitive=True,
                      aliases=['flip'])
    async def coinflip(self, ctx):
        choices = ["**heads**!","**tails**!"]
        rancoin = random.choice(choices)
        embed = discord.Embed(title="Results", description= f":coin: {ctx.author.mention} flipped a coin and it landed on "+random.choice(choices),colour=discord.Colour(0xad1457))
        await ctx.send(embed=embed)

    @commands.command(description="Gives profile picture",
                      case_insensitive=True,
                      aliases=['pfp'])
    async def avatar(self, ctx, avamember : Optional[discord.Member] = None):
         if not avamember:
             await ctx.send("Please provide a valid member!")
             return
         else:

             userAvatarUrl = avamember.avatar_url
             member = ctx.message.author

             color = avamember.top_role.colour
             embed = discord.Embed(color=color)

             author = "{}#{}".format(member.name, member.discriminator)
             embed.set_author(name=avamember)

             embed.description="[Web link]({link})".format(link=avamember.avatar_url_as(static_format="png"))
             embed.set_image(url=avamember.avatar_url)
             embed.set_footer(text="Requested by: {}".format(member))
             await ctx.send(embed=embed)

def setup(bot):
    """Basic setup for FunPlugin"""
    bot.add_cog(FunPlugin(bot))
