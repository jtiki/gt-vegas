"""Searcher module provides the SearcherPlugin Class and setup utilities"""
import datetime

import discord
from discord import Member
from discord.ext.commands import Context
from discord.ext.commands import MissingRequiredArgument
from discord.ext.commands import group
from discord.ext.commands import has_any_role
from discord.ext.commands import guild_only
from discord.ext.commands import MemberConverter

from settings.base import APPROVED_ROLES

from .base import BasePlugin


class SearchPlugin(BasePlugin):
    """Provides two primary methods to search statistics on a user, and four methods to modify and refine the search."""

    def __init__(self, bot):
        """Sets a reference to bot, and initializes default attributes"""
        super().__init__(bot)
        self.bot = bot
        self._ignored_channels = list()
        self._check_words = ["hey", "hi", "howdy"]

    @group(description="A search plugin for Approved uses", case_insensitive=True)
    @guild_only()
    @has_any_role(*APPROVED_ROLES)
    async def search(self, context: Context):
        """Group command with no use"""
        if context.invoked_subcommand is None:
            await context.send("Please use a subcommand")

    @search.command(
        description="Set channels that the bot should not search.",
        case_insensitive=True,
    )
    async def set_ignored_channels(self, context: Context, *channels):
        """Sets a list of ignored discord.Channel.mention's to ignore during the search.

        Parameters
        ----------
        context: Context
            Default discord.Context.
        *channels: List[TextChannel.mention]
        """
        self._ignored_channels = channels
        await context.send(":thumbsup:")

    @search.command(description="Show ignored channels.", case_insensitive=True)
    async def list_ignored_channels(self, context: Context):
        """Displays a bullet list of ignored channels.

        Parameters
        ----------
        context: Context
            Default discord.Context"""
        out = ""
        for channel in self._ignored_channels:
            out += f"\n- {channel}"
        await context.send(out)

    @search.command(description="Sets the words to check for")
    async def set_check_words(self, context: Context, *words):
        """Sets the words for the hell-mode check

        Parameters
        ----------
        context: Context
            Default discord.Context
        *words: List[str]
            A list of words to be checked for in the hell-mode search"""
        self._check_words = words
        await context.send(":thumbsup:")

    @set_check_words.error
    async def set_check_words_error(self, context, error):
        """Error checker"""
        if isinstance(error, MissingRequiredArgument):
            arg = error.param.name
            await context.send(arg)

    @search.command(
        description="Show the words the bot checks for.", case_insensitive=True
    )
    async def list_check_words(self, context):
        """Displays a bullet list of the check words for hell-mode search."""
        out = ""
        for word in self._check_words:
            out += f"\n- {word}"
        await context.send(out)

    @search.command(
        description="Searches if the given users have used the challenge words",
        case_insensitive=True,
        aliases=[
            "multi-hell-mode-search",
            "mhms",
        ],
    )
    async def multi_hell_mode_search(self, context: Context, after, *members: Member):
        """Searches all messages not in _ignored_channels for _check_words after `after` for each `member` in members

        Parameters
        ----------
        context: Context
            Default discord.Context
        after: str
            A string in YYYY-MM-DD format to get messages *after*
        *members: List[Member]
            A list of members to check the search against."""
        after_dt = datetime.datetime.strptime(after, "%Y-%m-%d")
        new_line = "\n"
        async with context.typing():
            messages_dict = dict()
            for channel in context.guild.text_channels:
                if channel.mention in self._ignored_channels:
                    pass
                else:
                    async for message in channel.history(limit=None, after=after_dt):
                        if any(message.author.id == member.id for member in members):
                            if any(
                                word.lower() in message.content.lower()
                                for word in self._check_words
                            ):
                                if messages_dict.get(message.author.id):
                                    messages_dict[message.author.id].append(
                                        [
                                            message.content,
                                            message.jump_url,
                                        ]
                                    )
                                else:
                                    messages_dict[message.author.id] = [
                                        [
                                            message.content,
                                            message.jump_url,
                                        ],
                                    ]
            for member_id, caught_list in messages_dict.items():
                member = await MemberConverter().convert(
                    ctx=context, argument=str(member_id)
                )
                embed = discord.Embed(title=f"{member.name} Report")
                for i, caught in enumerate(caught_list):
                    embed.add_field(
                        name=f"Caught {i}",
                        value=f"{caught[0][0:100]}...{new_line}{caught[1]}",
                        inline=False,
                    )
                await context.send(embed=embed)

    @multi_hell_mode_search.error
    async def multi_hell_mode_search_error(self, context, error):
        """Error handler for multi_hell_mode_search"""
        if isinstance(error, MissingRequiredArgument):
            arg = error.param.name
            if arg == "members":
                await context.send("Please provide one or more members!")
            elif arg == "after":
                await context.send("Please provide a date in the format 'yyyy-mm-dd'")
            else:
                await context.send(f"Missing parameter: {arg}")

    @search.command(
        description="Searches if a given user has used the challenge words",
        case_insensitive=True,
        aliases=[
            "hell-mode-search",
            "hms",
        ],
    )
    async def hell_mode_search(self, context: Context, member: Member, after):
        """Searches all messages not in _ignored_channels for _check_words after `after` form `member`

        Parameters
        ----------
        context: Context
            Default discord.Context
        member: Member
            A member to check the search against.
        after: str
            A string in YYYY-MM-DD format to get messages *after*"""
        after_dt = datetime.datetime.strptime(after, "%Y-%m-%d")
        async with context.typing():
            messages_checked = 0
            messages_from_author = 0
            messages_contained_word = 0
            await context.send(
                f"Searching for if {member.mention} has said {self._check_words} after {after}"
            )
            for channel in context.guild.text_channels:
                if channel.mention in self._ignored_channels:
                    pass
                else:
                    async for message in channel.history(limit=None, after=after_dt):
                        if message.author.id == member.id:
                            messages_from_author += 1
                            if any(
                                word.lower() in message.content.lower()
                                for word in self._check_words
                            ):
                                messages_contained_word += 1
                                await context.send(
                                    f"User said `{message.content}` at {context.message.jump_url}"
                                )
            await context.send(
                f"I checked {messages_checked} messages, {messages_from_author} messages were from {member.mention}, {messages_contained_word} messages contained the check words"
            )

    @hell_mode_search.error
    async def hell_mode_search_error(self, context, error):
        """Error handler for hell_mode_search"""
        if isinstance(error, MissingRequiredArgument):
            arg = error.param.name
            if arg == "member":
                await context.send("Please provide a member!")
            elif arg == "after":
                await context.send("Please provide a date in the format 'yyyy-mm-dd'")
            else:
                await context.send(f"Missing parameter: {arg}")


def setup(bot):
    """Default setup handle for discordpy cogs."""
    bot.add_cog(SearchPlugin(bot))
