from typing import List, Union

import emoji
from checks import requires_redis, setup_check_redis
from discord import (Embed, Emoji, Guild, Member, Message, PartialEmoji,
                     RawReactionActionEvent, Role, TextChannel)
from discord.ext import commands, tasks
from discord.ext.commands import (Context, EmojiConverter, EmojiNotFound,
                                  Paginator, RoleConverter,
                                  TextChannelConverter)
from discord.utils import get
from emoji import UNICODE_EMOJI_ENGLISH
from settings.base import (COLORS, GET_ROLE_SQL, REMOVE_ROLE_SQL, ROLE_FILTER,
                           ROLE_PRONOUN_FILTER)

from .base import BasePlugin


class AutoRolesPlugin(BasePlugin):
    def __init__(self, bot):
        super().__init__(bot)
        self.bot = bot

    async def _generate_embed_output(
        self,
        guild: Guild,
        embed_type: str,
        role: Role,
        user: Member,
        reason: str,
    ) -> dict:
        """Private helper command to create an embed explaining how the users roles
        have been updated

        Parameters
        ----------
        context: discord.ext.commands.Context
            A discord Context object.
        embed_type: str
            A string to identify what kind of embed to make.
        role: discord.Role
            The role that has been changed.
        user: discord.Member
            The user that has been changed.
        reason: str
            The reason to display.

        Returns
        -------
        dict
            A dictionary representing the embed object.
        """

        if embed_type == "give":
            action = "Given"
            color = COLORS["role_added"]
        elif embed_type == "temp_give":
            action = "Temporarily given"
            color = COLORS["role_added"]
        elif embed_type == "join":
            action = "Joined"
            color = COLORS["role_added"]
        elif embed_type == "remove":
            action = "Removed"
            color = COLORS["role_removed"]
        elif embed_type == "leave":
            action = "Left"
            color = COLORS["role_removed"]

        return {
            "color": color,
            "author": {
                "name": "{} roles updated!".format(user.name),
                "icon_url": str(guild.icon_url),
            },
            "thumbnail": {
                "url": str(user.avatar_url),
            },
            "fields": [
                {
                    "name": "Roles {}".format(action),
                    "value": str(role.name),
                },
                {
                    "name": "Reason:",
                    "value": reason,
                },
            ],
            "footer": {
                "icon_url": str(self.bot.user.avatar_url),
                "text": "Thanks for using {}!".format(self.bot.user.name),
            },
        }

    @property
    def emote_role_mapping_key(self) -> str:
        return "{guild_id}:roles:emote_role_hash:{role_type}"

    def _full_role_key(self, guild: Guild, role: Role):
        is_pronoun = any(
            role_filter.lower() in role.name.lower()
            for role_filter in ROLE_PRONOUN_FILTER
        )

        if is_pronoun:
            return self.emote_role_mapping_key.format(
                guild_id=guild.id, role_type="pronouns"
            )
        else:
            return self.emote_role_mapping_key.format(
                guild_id=guild.id, role_type="roles"
            )

    @property
    def _emoji_list(self) -> List[str]:
        return list(UNICODE_EMOJI_ENGLISH.values())

    def _pronoun_roles_list(self, guild: Guild) -> List[Role]:
        return [
            role.name
            for role in guild.roles
            if role.name.lower() in ROLE_PRONOUN_FILTER
        ]

    def _roles_list(self, guild: Guild) -> List[Role]:
        role_filters = list(set(ROLE_FILTER) - set(ROLE_PRONOUN_FILTER))

        return [
            role.name
            for role in guild.roles
            if any(role_filter in role.name.lower() for role_filter in role_filters)
        ]

    @commands.group(
        case_insensitive=True,
        aliases=[
            "ar",
        ],
    )
    @requires_redis()
    @commands.has_any_role("Mods", "Disaster Director")
    async def auto_roles(self, context: Context):
        if not message.guild:
            return
        if context.invoked_subcommand is None:
            await context.send("...")

    @auto_roles.command(
        aliases=[
            "+",
        ]
    )
    async def add_role(self, context: Context, emote: str, *, arole: Role):
        redis_key = self._full_role_key(context.guild, arole)

        if emoji.emoji_lis(emote):
            emote = emoji.demojize(emote).strip(":")

        else:
            emote = await EmojiConverter().convert(ctx=context, argument=str(emote))
            emote = emote.name

        await self.bot.redis.hmset(redis_key, emote, arole.name)
        await context.send(f"Set :{emote}: for {arole.name} on AutoRoles")

    @auto_roles.group(
        aliases=[
            "ac",
        ]
    )
    async def allowed_channels(self, context: Context):
        if context.invoked_subcommand is None:
            await context.send("...")

    @allowed_channels.command(
        aliases=[
            "a",
            "+",
        ]
    )
    async def add(self, context: Context, *, channel: TextChannel):
        redis_key = self.emote_role_mapping_key.format(
            guild_id=context.guild.id, role_type="allowed-channels"
        )
        await self.bot.redis.sadd(redis_key, channel.name)
        await context.send(f"Added {channel.mention} to work with AutoRoles!")

    @allowed_channels.command(
        aliases=[
            "r",
            "-",
        ]
    )
    async def remove(self, context: Context, *, channel: TextChannel):
        redis_key = self.emote_role_mapping_key.format(
            guild_id=context.guild.id, role_type="allowed-channels"
        )
        await self.bot.redis.srem(redis_key, 0, channel.name)
        await context.send(f"Removed {channel.mention} to work with AutoRoles!")

    @allowed_channels.command(aliases=["l", "list"])
    async def allowed_channels_list(self, context: Context):
        redis_key = self.emote_role_mapping_key.format(
            guild_id=context.guild.id, role_type="allowed-channels"
        )
        channel_names = await self.bot.redis.smembers(redis_key, encoding="utf-8")
        embed = Embed(
            title="Allowed Channels for AutoRoles", description="-", color=000
        )
        embed.set_thumbnail(url=context.guild.icon_url)

        for i, channel_name in enumerate(channel_names):
            print("Gt channel convert")
            channel = await TextChannelConverter().convert(
                ctx=context, argument=channel_name
            )
            print("Gt ere")
            embed.add_field(name=f"Channel {i+1}", value=f"{channel.mention}")

        await context.send(embed=embed)

    @auto_roles.command(
        aliases=[
            "l",
        ]
    )
    async def list(self, context: Context):
        pronoun_key = self.emote_role_mapping_key.format(
            guild_id=context.guild.id, role_type="pronouns"
        )
        roles_key = self.emote_role_mapping_key.format(
            guild_id=context.guild.id, role_type="roles"
        )
        pronoun_role_map = await self.bot.redis.hgetall(pronoun_key, encoding="utf-8")
        roles_map = await self.bot.redis.hgetall(roles_key, encoding="utf-8")
        des = [
            f"Below is a list of Roles you can assign to yourself for {context.guild.name}!",
            "\n\nJust find a role below that you want, you can:\n",
            "- Send the emote next to the role\n",
            "- React to this message with the emote next to the role\n",
            "- Send the role name (case-insensitive)\n\n",
            "If you have any questions feel free to ask in #server-help!",
        ]
        des = "".join(des)
        field_paginator = Paginator(prefix="", suffix="", max_size=256)
        embed = Embed(
            title=f"AutoRoles for {context.guild.name}", description=des, color=000
        )
        embed.set_thumbnail(url=context.guild.icon_url)

        for emote, role_name in pronoun_role_map.items():
            role = await RoleConverter().convert(ctx=context, argument=role_name)
            try:
                emote = await EmojiConverter().convert(ctx=context, argument=emote)
            except EmojiNotFound:
                emote = emoji.emojize(f":{emote}:")
            field_paginator.add_line(line=f"{emote} - {role.mention}")

        for i, field_val in enumerate(field_paginator.pages):
            i += 1
            embed.add_field(name=f"Pronoun Roles {i}", value=field_val, inline=False)

        field_paginator.clear()

        for emote, role_name in roles_map.items():
            role = await RoleConverter().convert(ctx=context, argument=role_name)
            try:
                emote = await EmojiConverter().convert(ctx=context, argument=emote)
            except EmojiNotFound:
                emote = emoji.emojize(f":{emote}:")
            field_paginator.add_line(line=f"{emote} - {role.mention}")

        for i, field_val in enumerate(field_paginator.pages):
            i += 1
            embed.add_field(
                name=f"Notification roles {i}", value=field_val, inline=False
            )

        await context.send(embed=embed)

    @auto_roles.command(
        aliases=[
            "re",
        ]
    )
    async def remove_emote(self, context: Context, *, emote: Union[Emoji, str]):
        return_message = "Nothing found to remove..."
        role_key = self.emote_role_mapping_key.format(
            guild_id=context.guild.id, role_type="roles"
        )
        pronoun_key = self.emote_role_mapping_key.format(
            guild_id=context.guild.id, role_type="pronouns"
        )
        removed = False

        if isinstance(emote, Emoji):
            emote = emote.name

        role_exists = await self.bot.redis.hexists(role_key, emote)
        pronoun_exists = await self.bot.redis.hexists(pronoun_key, emote)

        if role_exists:
            removed = True
            role_name = await self.bot.redis.hmget(role_key, emote, encoding="utf-8")
            await self.bot.redis.hdel(role_key, emote)

        if pronoun_exists:
            removed = True
            role_name = await self.bot.redis.hmget(pronoun_key, emote, encoding="utf-8")
            await self.bot.redis.hdel(pronoun_key, emote)

        if removed:
            return_message = f"Removed {emote} - {role_name} from AutoRoles!"

        await context.send(return_message)

    @commands.Cog.listener("on_message")
    async def user_role_toggle_by_role_name(self, message: Message):
        if not message.guild:
            return
        if message.author.bot:
            return
        role = None
        allowed_channels_key = self.emote_role_mapping_key.format(
            guild_id=message.guild.id, role_type="allowed-channels"
        )
        ac_names = await self.bot.redis.smembers(allowed_channels_key, encoding="utf-8")
        roles_key = self.emote_role_mapping_key.format(
            guild_id=message.guild.id, role_type="roles"
        )
        pronouns_key = self.emote_role_mapping_key.format(
            guild_id=message.guild.id, role_type="pronouns"
        )
        role_names = await self.bot.redis.hgetall(roles_key, encoding="utf-8")
        pronoun_names = await self.bot.redis.hgetall(pronouns_key, encoding="utf-8")

        if message.channel.name not in ac_names:
            return

        if any(
            pronoun_name.lower() == message.content.lower()
            for pronoun_name in pronoun_names.values()
        ) or any(
            role_name.lower() == message.content.lower()
            for role_name in role_names.values()
        ):
            for server_role in message.guild.roles:
                if server_role.name.lower() == message.content.lower():
                    role = server_role
            await self._update_user_role_and_send_embed(role, message)

    @commands.Cog.listener("on_message")
    async def user_role_toggle_by_unicode_emoji(self, message: Message):
        if not message.guild:
            return
        if message.author.bot:
            return
        allowed_channels_key = self.emote_role_mapping_key.format(
            guild_id=message.guild.id, role_type="allowed-channels"
        )
        ac_names = await self.bot.redis.smembers(allowed_channels_key, encoding="utf-8")
        emoji_list = emoji.emoji_lis(message.clean_content)

        if message.channel.name not in ac_names:
            return

        if not emoji_list:
            return

        emote = emoji.demojize(emoji_list[0]["emoji"]).strip(":")
        await self._give_role_from_emote_name(message, emote)

    @commands.Cog.listener("on_message")
    async def user_role_toggle_custom_emoji(self, message: Message):
        if not message.guild:
            return
        if message.author.bot:
            return
        allowed_channels_key = self.emote_role_mapping_key.format(
            guild_id=message.guild.id, role_type="allowed-channels"
        )
        ac_names = await self.bot.redis.smembers(allowed_channels_key, encoding="utf-8")

        if message.channel.name not in ac_names:
            return

        try:
            emote_name = message.content.split(":")[1]
            emote = get(message.guild.emojis, name=emote_name)
        except IndexError:
            emote = None

        if emote:
            await self._give_role_from_emote_name(message, emote.name)

    async def _give_role_from_emote_name(self, message: Message, emote: str):
        role_name = ""
        roles_key = self.emote_role_mapping_key.format(
            guild_id=message.guild.id, role_type="roles"
        )
        pronouns_key = self.emote_role_mapping_key.format(
            guild_id=message.guild.id, role_type="pronouns"
        )
        role_exists = await self.bot.redis.hexists(roles_key, emote)
        pronouns_exists = await self.bot.redis.hexists(pronouns_key, emote)

        if role_exists:
            role_name = await self.bot.redis.hget(roles_key, emote, encoding="utf-8")

        if pronouns_exists:
            role_name = await self.bot.redis.hget(pronouns_key, emote, encoding="utf-8")

        if not role_name:
            await message.channel.send(
                f"Sorry {message.author.mention}, I can't find a role that matches the emote :{emote}:"
            )

        role = get(message.guild.roles, name=role_name)
        await self._update_user_role_and_send_embed(role, message)

    async def _update_user_role_and_send_embed(self, role: Role, message: Message):
        if any(role.name == author_role.name for author_role in message.author.roles):
            reason = f"Removed {role.name} from {message.author.name} - requested via AutoRoles"
            await message.author.remove_roles(role, reason=reason)
            embed = await self._generate_embed_output(
                message.guild, "leave", role, message.author, reason
            )
        else:
            reason = (
                f"Added {role.name} to {message.author.name} - requested via AutoRoles"
            )
            await message.author.add_roles(role, reason=reason)
            embed = await self._generate_embed_output(
                message.guild, "join", role, message.author, reason
            )
        embed = Embed().from_dict(embed)
        await message.channel.send(embed=embed, delete_after=6)

        if not message.pinned:
            await message.delete(delay=5)

    @commands.Cog.listener("on_raw_reaction_add")
    async def user_role_toggle_reaction(self, payload: RawReactionActionEvent) -> None:
        """Monitors reactions in bot-spam and vegas-logging to match against the the
        `role_list` returned by `self._build_embed_and_roles_lists` and adds the
        appropriate role to the user.

        Parameters
        ==========
        payload: discord.RawReactionActionEvent
            The payload to extract the reaction info from."""

        if not payload.guild_id:
            return
        channel = self.bot.get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        context = await self.bot.get_context(message)
        user = context.guild.get_member(payload.user_id)
        allowed_channels_key = self.emote_role_mapping_key.format(
            guild_id=message.guild.id, role_type="allowed-channels"
        )
        ac_names = await self.bot.redis.smembers(allowed_channels_key, encoding="utf-8")

        if message.channel.name not in ac_names:
            return

        if message.author.bot:
            return

        if message.content != "" and user.id != self.bot.user.id:
            return

        if not message.pinned:
            return

        message.author = payload.member
        await self._give_role_from_emote_name(message, payload.emoji.name)
        await message.clear_reactions()


def setup(bot):
    setup_check_redis(bot, __file__)
    bot.add_cog(AutoRolesPlugin(bot))
