"""Provides the DeleterPlugin"""
import datetime
from functools import partial

import discord
from checks import is_pinned_partial
from discord import Embed
from discord.ext import commands, tasks
from discord.ext.commands import Context, ExtensionFailed, has_any_role
from discord.utils import get
from settings.base import COLORS, NO_CHANNEL_MESSAGE
from settings.local_settings import DEBUG
from utils import to_relative_delta

COLOR = COLORS["ban"]


class DeleterPlugin(commands.Cog):
    """Provides useful functions for deleting and cleaning up TextChannels"""

    def __init__(self, bot):
        self.bot = bot
        self._bot_spam_started = None
        self._bot_spam_completed = None
        self._get_access_started = None
        self._get_access_completed = None
        self.autodeleter_bot_spam_loop.start()
        self.autodeleter_get_access_loop.start()

    @tasks.loop(minutes=5.0)
    async def autodeleter_bot_spam_loop(self):
        """Helper Function. A background loop that runs every 5 minutes.
        Checks every guild for a channel named "bot-spam" and deletes 200
        messages"""
        await self.bot.wait_until_ready()

        for guild in self.bot.guilds:
            try:
                bot_spam_chan = get(guild.text_channels, name="bot-spam")
                self._bot_spam_started = datetime.datetime.now()

                if bot_spam_chan:
                    is_pinned = partial(is_pinned_partial, True)
                    await bot_spam_chan.purge(limit=200, check=is_pinned, bulk=False)
                self._bot_spam_completed = datetime.datetime.now()
            except (discord.Forbidden, discord.HTTPException):
                no_bot_spam = NO_CHANNEL_MESSAGE.format(
                    guild.name, "bot-spam", self.bot.user.mention
                )

                if DEBUG:
                    print(no_bot_spam)
                else:
                    await self.bot.owner.send(no_bot_spam)

    @tasks.loop(minutes=5.0)
    async def autodeleter_get_access_loop(self):
        """Helper Function. A background loop that runs every 5 minutes.
        Checks every guild for a channel named "get-access" and deletes 200
        messages"""
        await self.bot.wait_until_ready()

        for guild in self.bot.guilds:
            try:

                get_access_chan = get(guild.text_channels, name="get-access")
                self._get_access_started = datetime.datetime.now()

                if get_access_chan:
                    is_pinned = partial(is_pinned_partial, True)
                    await get_access_chan.purge(limit=200, check=is_pinned, bulk=True)
                self._get_access_completed = datetime.datetime.now()
            except (discord.Forbidden, discord.HTTPException):
                no_get_access = NO_CHANNEL_MESSAGE.format(
                    guild.name, "get-access", self.bot.user.mention
                )

                if DEBUG:
                    print(no_get_access)
                else:
                    await self.bot.owner.send(no_get_access)

    @commands.command(
        name="last_auto",
        description="Deletes so many messages",
        aliases=[
            "adl",
        ],
    )
    @has_any_role("Can Delete")
    async def last_auto_loop_delete(self, context: Context):
        """Displays details on when the background deleter loop last ran."""
        embed = Embed(
            description="Stats on when the last Deleter auto loop ran!", color=COLOR
        )
        bot_spam_started = to_relative_delta(self._bot_spam_started)
        bot_spam_completed = to_relative_delta(self._bot_spam_completed)
        get_access_started = to_relative_delta(self._get_access_started)
        get_access_completed = to_relative_delta(self._get_access_completed)
        bot_spam_value = "*Started:* {}\n*Ended:* {}".format(
            bot_spam_started, bot_spam_completed
        )
        get_access_value = "*Started:* {}\n*Ended:* {}".format(
            get_access_started, get_access_completed
        )

        embed.set_author(
            name=self.bot.user.name + " Auto Deleter Loop!",
            icon_url=str(context.guild.icon_url),
        )
        embed.add_field(name="Bot Spam Loop:", value=bot_spam_value, inline=False)
        embed.add_field(name="Get Access Loop:", value=get_access_value, inline=False)
        await context.send(embed=embed)

    @commands.command(
        name="del_reload",
        description="Deletes so many messages",
        aliases=[
            "dr",
        ],
    )
    @has_any_role("Can Delete")
    async def del_reload(self, context):
        """Reloads the deleter plugin to restart the background
        loops."""
        try:
            self.bot.unload_extension("plugins.deleter")
            self.bot.load_extension("plugins.deleter")
        except ExtensionFailed:
            await context.send("Deleter failed to reload!")
        else:
            await context.send(":thumbsup:")

    @commands.command(
        name="delete",
        description="Deletes so many messages",
        aliases=[
            "d",
            "clear",
            "del",
        ],
    )
    @has_any_role("Can Delete")
    async def delete(self, context: Context, to_del: int = 50, keep_pins: bool = True):
        """Deletes messages from a channel

        Parameters
        ----------
        context: Context
            Default discord Context
        to_del: int = 50
            The number of messages to delete. Defaults to 50.
        keep_pins: bool = True
            If pins should be ignored or deleted. Defaults True."""
        is_pinned = partial(is_pinned_partial, keep_pins)
        deleted = await context.message.channel.purge(
            limit=to_del + 1, check=is_pinned, bulk=True
        )
        embed = Embed(title=f"Deleted {len(deleted)} messages!", color=COLOR)
        confirm = await context.send(embed=embed)
        await confirm.delete(delay=5)


def setup(bot):
    """Simple setup function to load plugin"""
    bot.add_cog(DeleterPlugin(bot))
