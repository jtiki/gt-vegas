from discord import Embed
from discord.ext import commands
from discord.ext.commands import Context
from settings.base import COLORS, LB

from .base import BasePlugin


class PluralityPlugin(BasePlugin):
    @commands.group(
        description="Gives a brief explaination of plurality and how it affects this server",
        case_insensitive=True,
        aliases=[
            "multiplicity",
            "pl",
            "p",
        ],
    )
    async def plurality(self, context: Context):
        description = f"Welcome to {context.guild.name}! {LB}It looks like you're curious about DID and OSDD! We aim to be as inclusive as possible, so we have a number of great resources here to help. Below are a links that might help!"
        embed = Embed(
            title="What is DID/OSDD?", description=description, color=COLORS["ban"]
        )
        embed.add_field(
            name="SciShow Video on Dissociative Identity Disorder:",
            value="Hank Green presents a [5 minute video](https://www.youtube.com/watch?v=l4hVtBV5o4s) going over the basics of Dissociative Identity Disorders and how the media misportrays them.",
            inline=False,
        )
        embed.add_field(
            name="Plurality Hub Carrd",
            value="The Heretic created a great [carrd site](https://plurality-hub.carrd.co/#directory) with great introductory material, such as terminology and etiquette.",
            inline=False,
        )
        embed.add_field(
            name="D-I-D You Know Graphic",
            value="tigrin on DeviantArt provides a [great explanation of Dissociative Disorders in an infographic comic](https://www.deviantart.com/tigrin/art/D-I-D-You-Know-58072489). They go over what DID is like and how Hollywood portrays it incorrectly.",
            inline=False,
        )
        embed.add_field(
            name="BetterTogether's DID/OSDD Casually and Visually Explained",
            value="BetterTogether's tumblr offers many great DID/OSDD resources, including their wonderful [DID/OSDD Casually Explained](https://clever-and-unique-name.tumblr.com/post/187704319719/didosdd-casually-explained-masterpost). Great explanations in a very simple to understand visual style!",
            inline=False,
        )
        embed.add_field(
            name="StarSystem's (Aster/Katie/Viv/Alex) Info Post",
            value=f"{context.guild.name}'s owner, {context.guild.owner.mention} has OSDD and has posted about [their experience #srs-guys](https://discord.com/channels/353694095220408322/428919368810889219/842190901353906186)",
            inline=False,
        )
        embed.add_field(
            name="Why are accounts with a 'BOT' tag talking?",
            value="These are called 'Proxied Messages' and created using a bot called TupperBox. These messages are really created/written by whoever the author is listed as, but since DID/OSDD Systems share one body and usually one Discord account, the bot helps more easily identify who is speaking/fronting. If you have DID/OSDD you can request access to the bot in #role-requests.",
            inline=False,
        )
        await context.send(embed=embed)

    @plurality.command(
        description="Provides a simple ground exercise.",
        case_insensitive=True,
        aliases=[
            "breathing",
            "calm",
            "grounding",
        ],
    )
    async def grounding_exercise(self, context: Context):
        description = "[Coping with Trauma-Related Dissociation](https://www.amazon.com/Coping-Trauma-Related-Dissociation-Interpersonal-Neurobiology/dp/039370646X/ref=pd_sbs_1/143-8050920-8375535) provides a break down of a great explaination of grounding exercises that anyone can do anywhere. \n\n We have provided a word-for-word copy of this process below:"
        embed = Embed(
            title="Learning to be Present, a Grounding Exercise",
            description=description,
            color=COLORS["ban"],
        )
        embed.add_field(
            name="Visual Grounding",
            value="Notice three objects that you see in the room and pay close attention to their details (shape, color, texture, size, etc.). Make sure you do not hurry through this part of the exercise. Let your eyes linger over each object. Name three characteristics of the object out loud to yourself, for example, 'It is blue. It is big. It is round.'",
            inline=False,
        )
        embed.add_field(
            name="Audio Grounding",
            value="Notice three sounds that you hear in the present (inside or outside of the room). Listen to their quality. Are they loud or soft, constant or intermittent, pleasant or unpleasant? Again, name three characteristics of the sound out loud to yourself, for example, 'It is loud, grating, and definitely unpleasant.'",
            inline=False,
        )
        embed.add_field(
            name="Tactile Grounding",
            value="Now touch three objects close to you and describe out loud to yourself how they feel, for example, rough, smooth, cold, warm, hard or soft, and so forth.",
            inline=False,
        )
        embed.add_field(
            name="Repeat",
            value="Return to the three objects that you have chosen to observe with your eyes. As you notice them, concentrate on the fact that you are here and now with these objects in the present, in this room. Next, notice the sounds and concentrate on the fact that you are here in this room with those sounds. Finally, do the same with the objects you have touched. You can expand this exercise by repeating it several times, three items for each sense, then two for each, then one, and then build it up again to three. You can also add new items to keep your practice fresh.",
            inline=False,
        )
        embed.add_field(name="Examples", value="------", inline=False)
        embed.add_field(
            name="Sight:",
            value="Look around the room for something (or even someone) that can help remind you that you are in the present, for example, a piece of clothing you are wearing that you like, a particular color or shape or texture, a picture on the wall, a small object, a book. Name the object to yourself out loud.",
            inline=False,
        )
        embed.add_field(
            name="Sound:",
            value="Use the sounds around you to help you really focus on the here and now. For example, listen to the normal everyday noises around you: the heat or air conditioning or refrigerator running, people talking, doors opening or closing, traffic sounds, birds singing, a fan blowing. You can remind yourself: 'These are the sounds of normal life all around me. I am safe. I am here.'",
            inline=False,
        )
        embed.add_field(
            name="Taste:",
            value="Carry a small item of food with you that has a pleasant but intense taste, for example, lozenges, mints, hard candy or gum, a piece of fruit such as an orange or banana. If you feel ungrounded,pop it into your mouth and focus on the flavor and the feel of it in your mouth to help you be more here and now.",
            inline=False,
        )
        embed.add_field(
            name="Smell:",
            value="Carry something small with you that has a pleasant smell, for example, a favorite hand lotion, perfume, aftershave, or an aromatic fruit such as an orange. When you start to feel spacey or otherwise not very present, a pleasant smell is a powerful reminder of the present.",
            inline=False,
        )
        embed.add_field(
            name="Touch:",
            value="Try one or more of the following touch exercises that feels good to you. Touch the chair or sofa on which you are sitting, or your clothes. Feel them with your fingers and be very aware of the textures and weight of the fabric. Try pushing on the floor with your feet, so that you can really feel the floor supporting you. Squeeze your hands together and let the pressure and warmth remind you that you are here and now. Press your tongue hard to the roof of your mouth. Cross your arms over your chest with your fingertips on your collar bones and pat your chest, alternating left and right, reminding yourself that you are in the present and safe (the butterfly hug, Artigas & Jarero, 2005).",
            inline=False,
        )
        embed.add_field(
            name="Breathing",
            value="The way in which we breathe is crucial in helping us to be present. When people dissociate or space out, they are usually breathing very shallowly and rapidly or hold their breath too long. Take time to slow and regulate your breathing. Breathe in through your nose to a slow count of three, hold to the count of three, and then breathe out through your mouth to a slow count of three. Do this several times while being mindful of how you breathe. Notice whether there are already ways in which you ground yourself in the present.",
            inline=False,
        )
        embed.set_thumbnail(url=context.guild.icon_url)
        await context.send(embed=embed)

def setup(bot):
    bot.add_cog(PluralityPlugin(bot))
