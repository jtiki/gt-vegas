"""This module provides LoaderPlugin, a plugin to load and reload modules."""
from discord.ext import commands
from discord.ext.commands import (
    ExtensionAlreadyLoaded,
    ExtensionFailed,
    ExtensionNotFound,
    NoEntryPointError,
    is_owner,
)

from .base import BasePlugin


class LoaderPlugin(BasePlugin):
    """This plugin provides methods to load, unload and reload modules."""

    @commands.command(hidden=True)
    @is_owner()
    async def load(self, context, module: str):
        """Loads a module."""
        try:
            self.bot.load_extension("plugins." + module)
        except ExtensionAlreadyLoaded:
            await context.send("This plugin is already loaded... Reloading now...")
            reload_cmd = self.bot.get_command("reload")
            await context.invoke(reload_cmd, module)
        except (ExtensionNotFound, ExtensionFailed, NoEntryPointError) as error:
            await context.send("\N{PISTOL}")
            await context.send("{}: {}".format(type(error).__name__, error))
        else:
            await context.send("\N{OK HAND SIGN}")

    @commands.command(hidden=True)
    @is_owner()
    async def unload(self, context, module: str):
        """Unloads a module."""
        try:
            self.bot.unload_extension("plugins." + module)
        except (ExtensionNotFound, ExtensionFailed, NoEntryPointError) as error:
            await context.send("\N{PISTOL}")
            await context.send("{}: {}".format(type(error).__name__, error))
        else:
            await context.send("\N{OK HAND SIGN}")

    @commands.command(name="reload", hidden=True)
    @is_owner()
    async def _reload(self, context, module: str):
        """Reloads a module."""
        try:
            self.bot.unload_extension("plugins." + module)
            self.bot.load_extension("plugins." + module)
        except (ExtensionNotFound, ExtensionFailed, NoEntryPointError) as error:
            await context.send("\N{PISTOL}")
            await context.send("{}: {}".format(type(error).__name__, error))
        else:
            await context.send("\N{OK HAND SIGN}")


def setup(bot):
    """Simple seutp for LoaderPlugin"""
    bot.add_cog(LoaderPlugin(bot))
