import datetime
from typing import Union

from discord import Embed, Member, User
from exceptions.suggestions import MissingEmbedError


class SuggestionModel:
    def __init__(self):
        self._author = None
        self._submission_date = None

    @property
    def author(self):
        if not self._author:
            return None
        return self._author

    @author.setter
    def author(self, user: Union[Member, User]):
        if not isinstance(user, (User, Member)):
            raise ValueError(self.__class__.__name__ + ".author must be a User or Member")
        self._author = user

    @property
    def submission_date(self):
        if not self._submission_date:
            return None
        return self._submission_date

    @submission_date.setter
    def submission_date(self, date: str):
        """
        Raises
        ------
            ValueError - if `date` does not match MM DD YY format.
        """
        try:
            self._submission_date = datetime.datetime.strptime(date, "%b %d %y")
        except ValueError:
            raise ValueError(self.__class__.__name__ + ".submission_date requires a date format MM DD YY.")

    def validate(self) -> dict:
        validation_err = dict()
        if not self.author:
            validation_err['author'] = " not set"
        if not self.submission_date:
            validation_err['submission_date'] = " not set"

        return validation_err


class UserSuggestionModel(SuggestionModel):
    def __init__(self):
        super().__init__()
        self._suggested_user = None
        self._reason = None


    @property
    def suggested_user(self):
        return self._suggested_user

    @suggested_user.setter
    def suggested_user(self, user: Union[Member, User]):
        if not isinstance(user, (User, Member)):
            raise ValueError(self.__class__.__name__ + ".suggested_user must be a User or Member")
        self._suggested_user = user

    @property
    def reason(self):
        return self._reason

    @reason.setter
    def reason(self, reason: str):
        """
        Raises
        ------
            ValueError - if `reason` does not match MM DD YY format.
        """
        if len(reason) < 40:
            raise ValueError(self.__class__.__name__ + ".reason must have at least 40 characters!")
        self._reason = reason

    def validate(self) -> dict:
        validation_err = dict()
        if not self.suggested_user:
            validation_err['suggested_user'] = " not set"
        if not self.reason:
            validation_err['reason'] = " not set"
        super().validate()

        return validation_err

    @classmethod
    def from_embed(cls, embed: Embed):
        r = cls()
        return r


class OtherSuggestionModel(SuggestionModel):
    def __init__(self):
        super().__init__()
        self._suggestion = None
        self._what_is_it = None
        self._why = None
        self._other = None
        self._attachment = None

    @property
    def suggestion(self):
        return self._suggestion

    @suggestion.setter
    def suggestion(self, suggestion: str):
        if len(suggestion) < 30 or len(suggestion) > 300:
            raise ValueError(self.__class__.__name__ + ".suggestion must be more than 30, but less than 300 characters.")
        self._suggestion = suggestion

    @property
    def what_is_it(self):
        return self._what_is_it

    @what_is_it.setter
    def what_is_it(self, what_is_it: str):
        if len(what_is_it) < 80 or len(what_is_it) > 1300:
            raise ValueError(self.__class__.__name__ + ".what_is_it must be more than 80, but less than 1300 characters.")
        self._what_is_it = what_is_it

    @property
    def why(self):
        return self._why

    @why.setter
    def why(self, why: str):
        if len(why) < 30 or len(why) > 300:
            raise ValueError(self.__class__.__name__ + ".why must be more than 80, but less than 1300 characters.")
        self._why = why

    @property
    def other(self):
        return self._other

    @other.setter
    def other(self, other: str):
        if len(other) > 300:
            raise ValueError(self.__class__.__name__ + ".other must be more than 80, but less than 1300 characters.")
        self._other = other

    @property
    def attachment(self):
        return self._attachment

    @attachment.setter
    def attachment(self, attachment: str):
        self._attachment = attachment
