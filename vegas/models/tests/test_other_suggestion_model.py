import pytest
from pytest import fixture

from ..suggestions import OtherSuggestionModel


@fixture
def suggestion_model():
    return OtherSuggestionModel()

def test_suggestion_less_than_30_raises_value_error(suggestion_model):
    with pytest.raises(ValueError):
        suggestion_model.suggestion = "a"*29

def test_suggestion_allows_between_30_to_300(suggestion_model):
        suggestion_model.suggestion = "a"*30
        suggestion_model.suggestion = "a"*200
        suggestion_model.suggestion = "a"*300

def test_suggestion_greater_than_300_raises_value_error(suggestion_model):
    with pytest.raises(ValueError):
        suggestion_model.suggestion = "a"*301

def test_what_is_it_less_than_80_raises_value_error(suggestion_model):
    with pytest.raises(ValueError):
        suggestion_model.what_is_it = "a"*79

def test_what_allows_between_80_to_1300(suggestion_model):
        suggestion_model.what_is_it = "a"*80
        suggestion_model.what_is_it = "a"*800
        suggestion_model.what_is_it = "a"*1300

def test_what_is_it_greater_than_1300_raises_value_error(suggestion_model):
    with pytest.raises(ValueError):
        suggestion_model.what_is_it = "a"*1301

def test_why_less_than_30_raises_value_error(suggestion_model):
    with pytest.raises(ValueError):
        suggestion_model.why = "a"*29

def test_why_allows_between_30_to_300(suggestion_model):
        suggestion_model.why = "a"*30
        suggestion_model.why = "a"*200
        suggestion_model.why = "a"*300

def test_why_greater_than_300_raises_value_error(suggestion_model):
    with pytest.raises(ValueError):
        suggestion_model.why = "a"*301

def test_suggestion_allows_greater_than_30_less_than_300(suggestion_model):
    suggestion_model.suggestion = "a"*40
    suggestion_model.suggestion = "a"*140
    suggestion_model.suggestion = "a"*220
    suggestion_model.suggestion = "a"*299

def test_suggestion_validates(suggestion_model):
    raise NotImplemented("Whoops!")
