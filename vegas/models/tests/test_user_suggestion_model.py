import pytest
from pytest import fixture

from ..suggestions import UserSuggestionModel


@fixture
def suggestion_model():
    return UserSuggestionModel()

def test_suggested_user_raises_value_error(suggestion_model):
    with pytest.raises(ValueError):
        suggestion_model.suggested_user = "Just a String"

def test_reason_less_than_40_raises_value_error(suggestion_model):
    with pytest.raises(ValueError):
        suggestion_model.reason = "a"*39

def test_reason_allows_great_than_40(suggestion_model):
    suggestion_model.reason = "a"*40
