import pytest
from pytest import fixture

from ..suggestions import SuggestionModel


@fixture
def suggestion_model():
    return SuggestionModel()

def test_author_raises_value_error(suggestion_model):
    with pytest.raises(ValueError):
        suggestion_model.author = "Just a String"


def test_submission_date_raises_value_error(suggestion_model):
    with pytest.raises(ValueError):
        suggestion_model.submission_date = "06 02 2021"

def test_submission_date_sets(suggestion_model):
    suggestion_model.submission_date = "Jun 02 21"
