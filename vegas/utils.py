"""Multiple helper functions for VEGAS"""
import asyncio
import datetime
import typing
from typing import List, Optional, Tuple, Union

import discord
from blurple.io import MessageReply, ReactionAddReply
from discord import Attachment, Embed, Guild, Member, User
from discord.ext.commands import Context
from discord.utils import get

from settings.base import ACCEPT_EMOJI, CANCEL_EMOJI, COLORS


class MessageReplyDM(MessageReply):
    """Blurple MessageReply for DM's."""

    async def on_reply_attempt(self, reply: discord.Message):
        """Default blruple implementation deletes invalid user input. Bot's do not have this permission in DM's"""


class ReactionAddReplyDM(ReactionAddReply):
    """Blurple ReactionAddReply for DM's."""

    async def on_reply_attempt(self, payload: discord.RawReactionActionEvent):
        """Default blruple implementation removes user reaction. Bot's do not have this permission in DM's"""

    async def on_reply_complete(self):
        """Default blruple implementation removes all reactions. Bot's do not have this permission in DM's"""


def make_user_embed(user: User) -> Embed:
    user_disc = f"{user.name}#{user.discriminator}"
    embed = Embed(
        title=user_disc,
        description=f"ID: {user.id}\nNickname: {user.nick}",
    )
    embed.set_thumbnail(url=user.avatar_url)
    return embed


def make_guild_embed(guild: Guild) -> Embed:
    embed = Embed(title=guild.name, description=guild.description)
    embed.add_field(
        name="Owner:",
        value=f"{guild.owner.name}#{guild.owner.discriminator}",
        inline=True,
    )
    embed.add_field(name="Guild ID:", value=f"{guild.id}", inline=True)
    embed.add_field(name="Created At:", value=f"{guild.created_at}", inline=True)
    embed.add_field(name="Member Count:", value=f"{guild.member_count}", inline=False)
    embed.set_thumbnail(url=guild.icon_url)
    return embed


def asnyc_to_sync(coro: typing.Coroutine) -> typing.Any:
    """Allows async functions to run in non-async functions"""
    task = asyncio.create_task(coro)
    asyncio.get_running_loop().run_until_complete(task)

    return task.result()


def to_relative_delta(given_time: datetime.datetime):
    "Returns a string representing the time relative to now" ""
    now = datetime.datetime.now()
    delta = now - given_time
    seconds = abs(int(delta.seconds))
    seconds += delta.days * 86400
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    # sets additional return parameters
    day = week = month = year = new = False

    if days == 0:
        new = True
    elif days >= 365:
        year = True
    elif days >= 31:
        month = True
    elif days >= 7:
        week = True
    elif days < 7:
        day = True
    # returns appropriate output string and bools for special Account age cases

    if days > 0:
        return "{} day(s) ago".format(days), new, day, week, month, year

    if hours > 0:
        return (
            "{} hour(s) and {} minute(s) ago".format(hours, minutes),
            new,
            day,
            week,
            month,
            year,
        )

    if minutes > 0:
        return "{} minute(s) ago".format(minutes), new, day, week, month, year

    if seconds > 0:
        return "{} second(s) ago".format(seconds), new, day, week, month, year

    return "bruh idek;\n now: {}\ngiven: {}\ndelta: {}".format(now, given_time, delta)


def process_timeout(timeout: str) -> Tuple[datetime.datetime, str]:
    """Processes a shorthand relative time format into a dateime
    object and returns the object and a string explaination of the
    format (e.g. week, month).

    Parameters
    ----------
    timeout: str
        The time format to use for the offest.

    Returns
    -------
    removal_time: datetime.datetime
        A datetime object representing when the role should be removed.
    time_format: str
        A string representation of the amount of time passing.
    """
    now = datetime.datetime.now()
    time_format = timeout[-1]
    time = int(timeout[:-1])

    if time_format == "m":
        time_offset = datetime.timedelta(minutes=time)
        time_format = "minutes"

    elif time_format == "h":
        time_offset = datetime.timedelta(hours=time)
        time_format = "hours"

    elif time_format == "d":
        time_offset = datetime.timedelta(days=time)
        time_format = "days"

    elif time_format == "w":
        time_offset = datetime.timedelta(weeks=time)
        time_format = "weeks"

    elif time_format == "M":
        mtime = time * 4
        time_offset = datetime.timedelta(weeks=mtime)
        time_format = "months"

    removal_time = now + time_offset

    return removal_time, time_format


def convert_shorthand_to_datetime(timeout: str) -> datetime.datetime:
    """Processes a shorthand relative time format into a dateime
    object and returns the object and a string explaination of the
    format (e.g. week, month).

    Parameters
    ----------
    timeout: str
        The time format to use for the offest.

    Returns
    -------
    removal_time: datetime.datetime
        A datetime object representing when the role should be removed.
    time_format: str
        A string representation of the amount of time passing.
    """
    time_format = timeout[-1]
    time = int(timeout[:-1])

    if time_format == "s":
        time_offset = datetime.timedelta(seconds=time)
        time_format = "seconds"
    elif time_format == "m":
        time_offset = datetime.timedelta(minutes=time)
        time_format = "minutes"

    elif time_format == "h":
        time_offset = datetime.timedelta(hours=time)
        time_format = "hours"

    elif time_format == "d":
        time_offset = datetime.timedelta(days=time)
        time_format = "days"

    elif time_format == "w":
        time_offset = datetime.timedelta(weeks=time)
        time_format = "weeks"

    elif time_format == "M":
        mtime = time * 4
        time_offset = datetime.timedelta(weeks=mtime)
        time_format = "months"

    return time_offset


async def _generate_suggestion_embed(
    submission: str,
    author: Member,
    guild: Guild,
    submission_date,
    attachments: Optional[List[Attachment]],
    sid=None,
) -> Embed:
    embed = discord.Embed(
        title="New Suggestion", color=COLORS["ban"], description=submission
    )
    embed.set_author(
        name=author.name + "#" + author.discriminator, icon_url=author.avatar_url
    )
    if sid:
        embed.add_field(name="ID:", value=sid)

    if attachments:
        embed.set_image(url=attachments[0].url)
        for attachment in attachments:
            embed.add_field(name="Attachment:", value=attachment.url, inline=False)

    embed.set_thumbnail(url=guild.icon_url)
    embed.add_field(name="Author:", value=author.mention, inline=True)
    embed.add_field(name="Submission Date:", value=submission_date, inline=True)

    return embed


async def _check_suggestion_type(context: Context) -> str:
    await context.send("What kind of suggestion would you like to make?")
    await context.send(
        f"(Reply with `User` to suggest a user for an approved role or `Other` to suggest something else. {CANCEL_EMOJI} exits the suggestion.)"
    )

    type_message = await MessageReplyDM(
        context, validate=["User", "Other", CANCEL_EMOJI]
    ).result()

    if type_message.content == CANCEL_EMOJI:
        await context.send("Cya!")
        return

    return type_message.content.lower()


async def _interactive_get_user_submission(context: Context, guild, open_roles):
    await context.send("What is the users ID you would like to suggest?")
    await context.send(
        "https://support.discord.com/hc/en-us/articles/206346498-Where-can-I-find-my-User-Server-Message-ID-"
    )
    user_message = await MessageReplyDM(context, validate=r"^()[0-9]{8,}|❌)$").result()

    if user_message.content.lower() == CANCEL_EMOJI:
        await context.send("Cya!")

        return

    user = get(guild.members, id=int(user_message.content))
    user_disc = f"{user.name}#{user.discriminator}"
    if not user:
        await context.send("I couldn't find a user with that ID!")
        return
    embed = make_user_embed(user)
    await context.send(embed=embed)

    await context.send("What role would you like to suggest this user for?")
    open_roles = "\n".join(open_roles)
    await context.send(f"The current open roles are:\n{open_roles}")
    role_message = await MessageReplyDM(
        context,
        validate=open_roles
        + [
            "❌",
        ],
    ).result()

    if user_message.content.lower() == CANCEL_EMOJI:
        await context.send("Cya!")

        return

    if get(guild.roles, name=role_message.content):
        role = get(guild.roles, name=role_message.content)
    else:
        await context.send("I couldn't find that role...")
        return

    await context.send(
        f"Please provide a brief explanation of {user_disc} they would be a good fit for the role."
        "\n\n(Note: Explanations must be detailed and longer than 30 characters. If you need more than 2000 characters, we recommend using a google doc.)"
    )
    reason = await MessageReplyDM(context, validate=r"^([\S\s]{30,300}|❌)$").result()

    return (f"**{user_disc}** for {role.name}\n**Reason:** {reason.content}", None)


async def _interactive_get_other_submission(
    context: Context,
) -> Tuple[str, Optional[List[Attachment]]]:
    await context.send(
        "**Suggestion**\n"
        "Quick description of the suggestion, ~1 phrase\n"
        "(Note: A minimum of 30 characters is required. A maximum of 300 is accepted.)"
    )
    suggestion = await MessageReplyDM(
        context, validate=r"^([\S\s]{30,300}|❌)$"
    ).result()

    await context.send(
        "**What is it?**\n"
        "Thorough explanation of your suggestion, different types of ideas require different levels of explanation, for emotes/commands and recommending people for roles it isn't needed"
        "(Note: A minimum of 30 characters are required. A maximum of 2300 is accepted.)"
    )
    long_suggestion = await MessageReplyDM(
        context, validate=r"^([\S\s]{30,2300}|❌)$"
    ).result()

    await context.send(
        "**Why do we need it?**\n"
        "Explain what good would it do on the server, why do you want this?"
        "(Note: A minimum of 30 characters is required. A maximum of 700 is accepted.)"
    )
    why = await MessageReplyDM(context, validate=r"^([\S\s]{30,700}|❌)$").result()

    await context.send(
        "**Anything else you would like to add?**\n"
        "Additional comments that would not fall under the other categories, if not just send 'no'."
        "(Note: A maximum of 700 is accepted.)"
    )
    additional = await MessageReplyDM(context, validate=r"^([\S\s]{,700}|❌)$").result()

    attachments = list()

    while True:
        await context.send("Do you have any attachments/uploads to provide?")
        await context.send(
            "(Note: To provide an attachment send `yes` and the attachemnt. Accepts 1 attachment at a time. If not, send `no`.)"
        )
        attachments_message = await MessageReplyDM(
            context,
            validate=[
                "yes",
                "no",
            ],
        ).result()

        if attachments_message.content == "no":
            break

        if attachments_message.attachments:
            attachments.append(attachments_message.attachments[0])

        await attachments_message.add_reaction(ACCEPT_EMOJI)
        await context.send("Attachment accepted!")

    suggestion_string = (
        f"**Suggestion:**\n {suggestion.content}\n\n"
        f"**What is it?**\n {long_suggestion.content}\n\n"
        f"**Why do we need it?**\n {why.content}\n\n"
        f"**Anything else you would like to add?**\n {additional.content}"
    )

    return (suggestion_string, attachments)
