"""Various exceptions for the VEGAS bot"""
from discord.ext.commands import CommandError


class PostgresNotInstalled(CommandError):
    """Error indicating that postgres is not configured for this bot, or not
    properly connected."""


class RedisNotInstalled(CommandError):
    """Error indicating that Redis is not configured for this bot, or not
    properly connected."""
