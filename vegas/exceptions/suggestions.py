from discord import Message, TextChannel
from discord.ext.commands import CheckFailure, CommandError


class MissingPinsError(CommandError):
    def __init__(self, channel: TextChannel, *args):
        self.channel= channel
        err_message = f"The channel you mentioned {channel.mention} doesn't seem to have any pins..."
        super().__init__(err_message, *args)

class MissingEmbedError(CommandError):
    def __init__(self, message: Message, *args):
        err_message = f"Provided Message (id: {message.id} - <{message.jump_url}>) does not have an embed."
        super().__init__(err_message, *args)


class NotVotingGuild(CheckFailure):
    pass
